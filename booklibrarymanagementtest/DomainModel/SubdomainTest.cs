﻿namespace BookLibraryManagementTest.SubdomainModel
{
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SubdomainTest
    {
        [TestMethod]
        public void TestSubdomainValidatorWithValidValues1()
        {
            Subdomain test = new Subdomain()
            {
                Id = 1,
                Name = "name"
            };

            SubdomainValidator validator = new SubdomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestSubdomainValidatorWithValidValues2()
        {
            Subdomain test = new Subdomain()
            {
                Id = 2,
                Name = "another name"
            };

            SubdomainValidator validator = new SubdomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestSubdomainValidatorWithValidValues3()
        {
            Subdomain test = new Subdomain()
            {
                Id = 3,
                Name = "yet another name"
            };

            SubdomainValidator validator = new SubdomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestSubdomainValidatorWithInvalidValues1()
        {
            Subdomain test = new Subdomain()
            {
                Id = 1
            };

            SubdomainValidator validator = new SubdomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestSubdomainValidatorWithInvalidValues2()
        {
            Subdomain test = new Subdomain()
            {
                Name = "name"
            };

            SubdomainValidator validator = new SubdomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestSubdomainValidatorWithInvalidValues3()
        {
            Subdomain test = new Subdomain();

            SubdomainValidator validator = new SubdomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestProperties()
        {
            Subdomain test = new Subdomain();

            test.Id = 1;
            test.Name = "Subdomain";

            Assert.AreEqual(test.Id, 1);
            Assert.AreEqual(test.Name, "Subdomain");
        }
    }
}
