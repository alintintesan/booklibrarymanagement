﻿namespace BookLibraryManagementTest.DomainModel
{
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class PublishingHouseTest
    {
        [TestMethod]
        public void TestPublishingHouseValidatorWithValidValues1()
        {
            PublishingHouse test = new PublishingHouse()
            {
                Id = 1,
                Name = "Publishing house"
            };

            PublishingHouseValidator validator = new PublishingHouseValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestPublishingHouseValidatorWithValidValues2()
        {
            PublishingHouse test = new PublishingHouse()
            {
                Id = 2,
                Name = "Another publishing house"
            };

            PublishingHouseValidator validator = new PublishingHouseValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestPublishingHouseValidatorWithValidValues3()
        {
            PublishingHouse test = new PublishingHouse()
            {
                Id = 3,
                Name = "Yet another publishing house"
            };

            PublishingHouseValidator validator = new PublishingHouseValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestPublishingHouseValidatorWithInvalidValues1()
        {
            PublishingHouse test = new PublishingHouse()
            {
                Id = 1
            };

            PublishingHouseValidator validator = new PublishingHouseValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestPublishingHouseValidatorWithInvalidValues2()
        {
            PublishingHouse test = new PublishingHouse()
            {
                Name = null
            };

            PublishingHouseValidator validator = new PublishingHouseValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestPublishingHouseValidatorWithInvalidValues3()
        {
            PublishingHouse test = new PublishingHouse();

            PublishingHouseValidator validator = new PublishingHouseValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestProperties()
        {
            PublishingHouse test = new PublishingHouse();

            test.Id = 1;
            test.Name = "PublishingHouse";

            Assert.AreEqual(test.Id, 1);
            Assert.AreEqual(test.Name, "PublishingHouse");
        }
    }
}
