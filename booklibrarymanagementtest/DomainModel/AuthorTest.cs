﻿namespace BookLibraryManagementTest.DomainModel
{
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class AuthorTest
    {
        [TestMethod]
        public void TestAuthorValidatorWithValidValues1()
        {
            Author test = new Author()
            {
                Id = 1,
                Name = "name"
            };

            AuthorValidator validator = new AuthorValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestAuthorValidatorWithValidValues2()
        {
            Author test = new Author()
            {
                Id = 2,
                Name = "another name"
            };

            AuthorValidator validator = new AuthorValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestAuthorValidatorWithValidValues3()
        {
            Author test = new Author()
            {
                Id = 3,
                Name = "yet another name"
            };

            AuthorValidator validator = new AuthorValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestAuthorValidatorWithInvalidValues1()
        {
            Author test = new Author()
            {
                Id = 1
            };

            AuthorValidator validator = new AuthorValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestAuthorValidatorWithInvalidValues2()
        {
            Author test = new Author()
            {
                Name = "name"
            };

            AuthorValidator validator = new AuthorValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestAuthorValidatorWithInvalidValues3()
        {
            Author test = new Author();

            AuthorValidator validator = new AuthorValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestProperties()
        {
            Author test = new Author();

            test.Id = 1;
            test.Name = "Name";

            Assert.AreEqual(test.Id, 1);
            Assert.AreEqual(test.Name, "Name");
        }
    }
}
