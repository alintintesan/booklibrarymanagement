﻿namespace BookLibraryManagementTest.LoanModel
{
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class LoanTest
    {
        [TestMethod]
        public void TestLoanValidatorWithValidValues1()
        {
            Loan test = new Loan()
            {
                Id = 1,
                Reader = new Reader()
            };

            LoanValidator validator = new LoanValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestLoanValidatorWithValidValues2()
        {
            Loan test = new Loan()
            {
                Id = 2,
                Reader = new Reader()
            };

            LoanValidator validator = new LoanValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestLoanValidatorWithValidValues3()
        {
            Loan test = new Loan()
            {
                Id = 3,
                Reader = new Reader()
            };

            LoanValidator validator = new LoanValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestLoanValidatorWithInvalidValues1()
        {
            Loan test = new Loan()
            {
                Id = 1
            };

            LoanValidator validator = new LoanValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestLoanValidatorWithInvalidValues2()
        {
            Loan test = new Loan()
            {
                Reader = null
            };

            LoanValidator validator = new LoanValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestLoanValidatorWithInvalidValues3()
        {
            Loan test = new Loan();

            LoanValidator validator = new LoanValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestProperties()
        {
            Loan test = new Loan();

            test.Id = 1;

            Assert.AreEqual(test.Id, 1);
        }
    }
}
