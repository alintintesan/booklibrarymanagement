﻿namespace BookLibraryManagementTest.DomainModel
{
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ReaderTest
    {
        [TestMethod]
        public void TestReaderValidatorWithValidValues1()
        {
            Reader test = new Reader()
            {
                Id = 1,
                Address = "address",
                Email = "Email",
                FirstName = "FirstName",
                LastName = "LastName",
                IsEmployee = false,
                PhoneNumber = "071234"
            };

            ReaderValidator validator = new ReaderValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestReaderValidatorWithValidValues2()
        {
            Reader test = new Reader()
            {
                Id = 2,
                Address = "address2",
                Email = "Email2",
                FirstName = "FirstName2",
                LastName = "LastName2",
                IsEmployee = true,
                PhoneNumber = "67546757"
            };

            ReaderValidator validator = new ReaderValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestReaderValidatorWithValidValues3()
        {
            Reader test = new Reader()
            {
                Id = 2,
                Address = "address5",
                Email = "Email2",
                FirstName = "FirstName5",
                LastName = "LastName5",
                IsEmployee = true,
                PhoneNumber = "1234567890"
            };

            ReaderValidator validator = new ReaderValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestReaderValidatorWithInvalidValues1()
        {
            Reader test = new Reader()
            {
                Id = 1,
                Address = "address",
                Email = "Email",
                FirstName = "FirstName",
                LastName = "LastName",
                IsEmployee = false,
                PhoneNumber = "1"
            };

            ReaderValidator validator = new ReaderValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestReaderValidatorWithInvalidValues2()
        {
            Reader test = new Reader()
            {
                Id = 1,
                Address = "ad",
                Email = "E",
                FirstName = "FirstName",
                LastName = "LastName",
                IsEmployee = false,
                PhoneNumber = "071234"
            };

            ReaderValidator validator = new ReaderValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestReaderValidatorWithInvalidValues3()
        {
            Reader test = new Reader();

            ReaderValidator validator = new ReaderValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestProperties()
        {
            Reader test = new Reader();

            test.Id = 1;
            test.Email = "email@example.com";

            Assert.AreEqual(test.Id, 1);
            Assert.AreEqual(test.Email, "email@example.com");
        }
    }
}
