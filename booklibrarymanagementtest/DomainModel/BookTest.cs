﻿namespace BookLibraryManagementTest.DomainModel
{
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BookTest
    {
        [TestMethod]
        public void TestBookValidatorWithValidValues1()
        {
            Book test = new Book()
            {
                Id = 1,
                Name = "name",
                IsForReadingRoomOnly = true
            };

            BookValidator validator = new BookValidator();
            FluentValidation.Results.ValidationResult results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestBookValidatorWithValidValues2()
        {
            Book test = new Book()
            {
                Id = 2,
                Name = "another name",
                IsForReadingRoomOnly = true
            };

            BookValidator validator = new BookValidator();
            FluentValidation.Results.ValidationResult results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestBookValidatorWithValidValues3()
        {
            Book test = new Book()
            {
                Id = 3,
                Name = "yet another name",
                IsForReadingRoomOnly = true
            };

            BookValidator validator = new BookValidator();
            FluentValidation.Results.ValidationResult results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestBookValidatorWithInvalidValues1()
        {
            Book test = new Book()
            {
                Id = 1
            };

            BookValidator validator = new BookValidator();
            FluentValidation.Results.ValidationResult results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestBookValidatorWithInvalidValues2()
        {
            Book test = new Book()
            {
                IsForReadingRoomOnly = true
            };

            BookValidator validator = new BookValidator();
            FluentValidation.Results.ValidationResult results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestBookValidatorWithInvalidValues3()
        {
            Book test = new Book();

            BookValidator validator = new BookValidator();
            FluentValidation.Results.ValidationResult results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestProperties()
        {
            Book test = new Book();

            test.Id = 1;
            test.Name = "Book";

            Assert.AreEqual(test.Id, 1);
            Assert.AreEqual(test.Name, "Book");
        }
    }
}
