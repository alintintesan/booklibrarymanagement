﻿namespace BookLibraryManagementTest.DomainModel
{
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class DomainTest
    {
        [TestMethod]
        public void TestDomainValidatorWithValidValues1()
        {
            Domain test = new Domain()
            {
                Id = 1,
                Name = "name"
            };

            DomainValidator validator = new DomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestDomainValidatorWithValidValues2()
        {
            Domain test = new Domain()
            {
                Id = 2,
                Name = "another name"
            };

            DomainValidator validator = new DomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestDomainValidatorWithValidValues3()
        {
            Domain test = new Domain()
            {
                Id = 3,
                Name = "yet another name"
            };

            DomainValidator validator = new DomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestDomainValidatorWithInvalidValues1()
        {
            Domain test = new Domain()
            {
                Id = 1
            };

            DomainValidator validator = new DomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestDomainValidatorWithInvalidValues2()
        {
            Domain test = new Domain()
            {
                Name = "name"
            };

            DomainValidator validator = new DomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestDomainValidatorWithInvalidValues3()
        {
            Domain test = new Domain();

            DomainValidator validator = new DomainValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestProperties()
        {
            Domain test = new Domain();

            test.Id = 1;
            test.Name = "Domain";

            Assert.AreEqual(test.Id, 1);
            Assert.AreEqual(test.Name, "Domain");
        }
    }
}
