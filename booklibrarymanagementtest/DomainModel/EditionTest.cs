﻿namespace BookLibraryManagementTest.DomainModel
{
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class EditionTest
    {
        [TestMethod]
        public void TestEditionValidatorWithValidValues1()
        {
            Edition test = new Edition()
            {
                Id = 1,
                NumberOfPages = 3,
                Type = "Type",
                PublishingHouse = new PublishingHouse()
            };

            EditionValidator validator = new EditionValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestEditionValidatorWithValidValues2()
        {
            Edition test = new Edition()
            {
                Id = 2,
                NumberOfPages = 33,
                Type = "Type",
                PublishingHouse = new PublishingHouse()
            };

            EditionValidator validator = new EditionValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestEditionValidatorWithValidValues3()
        {
            Edition test = new Edition()
            {
                Id = 10,
                NumberOfPages = 543,
                Type = "Type",
                PublishingHouse = new PublishingHouse()
            };

            EditionValidator validator = new EditionValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestEditionValidatorWithInvalidValues1()
        {
            Edition test = new Edition()
            {
                Id = 1
            };

            EditionValidator validator = new EditionValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestEditionValidatorWithInvalidValues2()
        {
            Edition test = new Edition()
            {
                NumberOfPages = 456
            };

            EditionValidator validator = new EditionValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestEditionValidatorWithInvalidValues3()
        {
            Edition test = new Edition();

            EditionValidator validator = new EditionValidator();
            var results = validator.Validate(test);

            bool isValid = results.IsValid;
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void TestProperties()
        {
            Edition test = new Edition();

            test.Id = 1;
            test.Type = "Type";

            Assert.AreEqual(test.Id, 1);
            Assert.AreEqual(test.Type, "Type");
        }
    }
}
