﻿namespace BookLibraryManagementTest.DomainModel
{
    using System;
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;
    using FluentValidation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BookComplexValidatorTest
    {
        [TestMethod]
        public void BookComplexValidatorValidTest1()
        {
            Book book = new Book()
            {
                Domains = new List<Domain>()
                {
                    new Domain()
                    {
                        Name = "Domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Subdomain"
                            }
                        }
                    }
                },
                Authors = new List<Author>() { new Author() }
            };

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void BookComplexValidatorValidTest2()
        {
            Book book = new Book()
            {
                Domains = new List<Domain>()
                {
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            },
                            new Subdomain()
                            {
                                Name = "Yet another subdomain"
                            }
                        }
                    }
                },
                Authors = new List<Author>() { new Author(), new Author() }
            };

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void BookComplexValidatorValidTest3()
        {
            Book book = new Book()
            {
                Domains = new List<Domain>()
                {
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            },
                            new Subdomain()
                            {
                                Name = "Yet another subdomain"
                            }
                        }
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            },
                            new Subdomain()
                            {
                                Name = "Yet another subdomain"
                            }
                        }
                    }
                },
                Authors = new List<Author>() { new Author(), new Author(), new Author(), new Author(), new Author(), new Author() }
            };

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsTrue(isValid);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void BookComplexValidatorInvalidWithEmptyObjectTest()
        {
            Book book = new Book();

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsFalse(isValid);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BookComplexValidatorInvalidWithNullObjectTest()
        {
            Book book = null;

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void BookComplexValidatorInvalidWithSameDomainAndSubdomainTest()
        {
            Book book = new Book()
            {
                Domains = new List<Domain>()
                {
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another domain"
                            },
                            new Subdomain()
                            {
                                Name = "Yet another subdomain"
                            }
                        }
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            },
                            new Subdomain()
                            {
                                Name = "Yet another subdomain"
                            }
                        }
                    }
                },
                Authors = new List<Author>() { new Author(), new Author(), new Author(), new Author(), new Author(), new Author() }
            };

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void BookComplexValidatorInvalidWithNoDomains()
        {
            Book book = new Book()
            {
                Domains = new List<Domain>(),
                Authors = new List<Author>() { new Author(), new Author(), new Author(), new Author(), new Author(), new Author() }
            };

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void BookComplexValidatorInvalidWithNoAuthors()
        {
            Book book = new Book()
            {
                Domains = new List<Domain>()
                {
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another domain"
                            },
                            new Subdomain()
                            {
                                Name = "Yet another subdomain"
                            }
                        }
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            },
                            new Subdomain()
                            {
                                Name = "Yet another subdomain"
                            }
                        }
                    }
                },
                Authors = new List<Author>() { }
            };

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void BookComplexValidatorInvalidWithLessThatOneSubdomainPerDomain()
        {
            Book book = new Book()
            {
                Domains = new List<Domain>()
                {
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            },
                            new Subdomain()
                            {
                                Name = "Yet another subdomain"
                            }
                        }
                    }
                },
                Authors = new List<Author>() { new Author(), new Author(), new Author(), new Author(), new Author(), new Author() }
            };

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void BookComplexValidatorInvalidWithMoreThanMaximumDomainsPerBook()
        {
            Book book = new Book()
            {
                Domains = new List<Domain>()
                {
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            }
                        }
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            }
                        }
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            }
                        }
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            }
                        }
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            }
                        }
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            }
                        }
                    },
                    new Domain()
                    {
                        Name = "Another domain",
                        Subdomains = new List<Subdomain>()
                        {
                            new Subdomain()
                            {
                                Name = "Another subdomain"
                            }
                        }
                    }
                },
                Authors = new List<Author>() { new Author(), new Author(), new Author(), new Author(), new Author(), new Author() }
            };

            AbstractValidator<Book> validator = new BookComplexValidator();
            bool isValid = validator.Validate(book).IsValid;

            Assert.IsFalse(isValid);
        }
    }
}
