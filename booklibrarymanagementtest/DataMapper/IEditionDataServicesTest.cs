﻿namespace BookLibraryManagementTest.DataMapper
{
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class IEditionDataServicesTest
    {
        [TestMethod]
        public void AddEditionTest()
        {
            Edition edition = new Mock<Edition>().Object;

            Mock<IEditionDataServices> mock = new Mock<IEditionDataServices>();
            mock.Setup(m => m.AddEdition(edition));

            IEditionDataServices obj = mock.Object;
            obj.AddEdition(edition);

            mock.Verify(o => o.AddEdition(edition), Times.Once());
        }

        [TestMethod]
        public void DeleteEditionTest()
        {
            Edition edition = new Mock<Edition>().Object;

            Mock<IEditionDataServices> mock = new Mock<IEditionDataServices>();
            mock.Setup(m => m.DeleteEdition(edition));

            IEditionDataServices obj = mock.Object;
            obj.DeleteEdition(edition);

            mock.Verify(o => o.DeleteEdition(edition), Times.Once());
        }

        [TestMethod]
        public void UpdateEditionTest()
        {
            Edition edition = new Mock<Edition>().Object;

            Mock<IEditionDataServices> mock = new Mock<IEditionDataServices>();
            mock.Setup(m => m.UpdateEdition(edition));

            IEditionDataServices obj = mock.Object;
            obj.UpdateEdition(edition);

            mock.Verify(o => o.UpdateEdition(edition), Times.Once());
        }

        [TestMethod]
        public void GetAllEditionsTest()
        {
            Mock<IEditionDataServices> mock = new Mock<IEditionDataServices>();
            mock.Setup(m => m.GetAllEditions());

            IEditionDataServices obj = mock.Object;
            obj.GetAllEditions();

            mock.Verify(o => o.GetAllEditions(), Times.Once());
        }

        [TestMethod]
        public void GetEditionByIdTest()
        {
            Mock<IEditionDataServices> mock = new Mock<IEditionDataServices>();
            mock.Setup(m => m.GetEditionById(1));

            IEditionDataServices obj = mock.Object;
            obj.GetEditionById(1);

            mock.Verify(o => o.GetEditionById(1), Times.Once());
        }
    }
}
