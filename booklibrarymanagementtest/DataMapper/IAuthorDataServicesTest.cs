﻿namespace BookLibraryManagementTest.DataMapper
{
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class IAuthorDataServicesTest
    {
        [TestMethod]
        public void AddAuthorTest()
        {
            Author author = new Mock<Author>().Object;

            Mock<IAuthorDataServices> mock = new Mock<IAuthorDataServices>();
            mock.Setup(m => m.AddAuthor(author));

            IAuthorDataServices obj = mock.Object;
            obj.AddAuthor(author);

            mock.Verify(o => o.AddAuthor(author), Times.Once());
        }

        [TestMethod]
        public void DeleteAuthorTest()
        {
            Author author = new Mock<Author>().Object;

            Mock<IAuthorDataServices> mock = new Mock<IAuthorDataServices>();
            mock.Setup(m => m.DeleteAuthor(author));

            IAuthorDataServices obj = mock.Object;
            obj.DeleteAuthor(author);

            mock.Verify(o => o.DeleteAuthor(author), Times.Once());
        }

        [TestMethod]
        public void UpdateAuthorTest()
        {
            Author author = new Mock<Author>().Object;

            Mock<IAuthorDataServices> mock = new Mock<IAuthorDataServices>();
            mock.Setup(m => m.UpdateAuthor(author));

            IAuthorDataServices obj = mock.Object;
            obj.UpdateAuthor(author);

            mock.Verify(o => o.UpdateAuthor(author), Times.Once());
        }

        [TestMethod]
        public void GetAllAuthorsTest()
        {
            Mock<IAuthorDataServices> mock = new Mock<IAuthorDataServices>();
            mock.Setup(m => m.GettAllAuthors());

            IAuthorDataServices obj = mock.Object;
            obj.GettAllAuthors();

            mock.Verify(o => o.GettAllAuthors(), Times.Once());
        }

        [TestMethod]
        public void GetAuthorByIdTest()
        {
            Mock<IAuthorDataServices> mock = new Mock<IAuthorDataServices>();
            mock.Setup(m => m.GetAuthorById(1));

            IAuthorDataServices obj = mock.Object;
            obj.GetAuthorById(1);

            mock.Verify(o => o.GetAuthorById(1), Times.Once());
        }
    }
}
