﻿namespace BookLibraryManagementTest.DataMapper
{
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class IBookDataServicesTest
    {
        [TestMethod]
        public void AddBookTest()
        {
            Book book = new Mock<Book>().Object;

            Mock<IBookDataServices> mock = new Mock<IBookDataServices>();
            mock.Setup(m => m.AddBook(book));

            IBookDataServices obj = mock.Object;
            obj.AddBook(book);

            mock.Verify(o => o.AddBook(book), Times.Once());
        }

        [TestMethod]
        public void DeleteBookTest()
        {
            Book book = new Mock<Book>().Object;

            Mock<IBookDataServices> mock = new Mock<IBookDataServices>();
            mock.Setup(m => m.DeleteBook(book));

            IBookDataServices obj = mock.Object;
            obj.DeleteBook(book);

            mock.Verify(o => o.DeleteBook(book), Times.Once());
        }

        [TestMethod]
        public void UpdateBookTest()
        {
            Book book = new Mock<Book>().Object;

            Mock<IBookDataServices> mock = new Mock<IBookDataServices>();
            mock.Setup(m => m.UpdateBook(book));

            IBookDataServices obj = mock.Object;
            obj.UpdateBook(book);

            mock.Verify(o => o.UpdateBook(book), Times.Once());
        }

        [TestMethod]
        public void GetAllBooksTest()
        {
            Mock<IBookDataServices> mock = new Mock<IBookDataServices>();
            mock.Setup(m => m.GetAllBooks());

            IBookDataServices obj = mock.Object;
            obj.GetAllBooks();

            mock.Verify(o => o.GetAllBooks(), Times.Once());
        }

        [TestMethod]
        public void GetBookByIdTest()
        {
            Mock<IBookDataServices> mock = new Mock<IBookDataServices>();
            mock.Setup(m => m.GetBookById(1));

            IBookDataServices obj = mock.Object;
            obj.GetBookById(1);

            mock.Verify(o => o.GetBookById(1), Times.Once());
        }
    }
}
