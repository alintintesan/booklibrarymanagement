﻿namespace DomainLibraryManagementTest.DataMapper
{
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class IDomainDataServicesTest
    {
        [TestMethod]
        public void AddDomainTest()
        {
            Domain domain = new Mock<Domain>().Object;

            Mock<IDomainDataServices> mock = new Mock<IDomainDataServices>();
            mock.Setup(m => m.AddDomain(domain));

            IDomainDataServices obj = mock.Object;
            obj.AddDomain(domain);

            mock.Verify(o => o.AddDomain(domain), Times.Once());
        }

        [TestMethod]
        public void DeleteDomainTest()
        {
            Domain domain = new Mock<Domain>().Object;

            Mock<IDomainDataServices> mock = new Mock<IDomainDataServices>();
            mock.Setup(m => m.DeleteDomain(domain));

            IDomainDataServices obj = mock.Object;
            obj.DeleteDomain(domain);

            mock.Verify(o => o.DeleteDomain(domain), Times.Once());
        }

        [TestMethod]
        public void UpdateDomainTest()
        {
            Domain domain = new Mock<Domain>().Object;

            Mock<IDomainDataServices> mock = new Mock<IDomainDataServices>();
            mock.Setup(m => m.UpdateDomain(domain));

            IDomainDataServices obj = mock.Object;
            obj.UpdateDomain(domain);

            mock.Verify(o => o.UpdateDomain(domain), Times.Once());
        }

        [TestMethod]
        public void GetAllDomainsTest()
        {
            Mock<IDomainDataServices> mock = new Mock<IDomainDataServices>();
            mock.Setup(m => m.GetAllDomains());

            IDomainDataServices obj = mock.Object;
            obj.GetAllDomains();

            mock.Verify(o => o.GetAllDomains(), Times.Once());
        }

        [TestMethod]
        public void GetDomainByIdTest()
        {
            Mock<IDomainDataServices> mock = new Mock<IDomainDataServices>();
            mock.Setup(m => m.GetDomainById(1));

            IDomainDataServices obj = mock.Object;
            obj.GetDomainById(1);

            mock.Verify(o => o.GetDomainById(1), Times.Once());
        }
    }
}
