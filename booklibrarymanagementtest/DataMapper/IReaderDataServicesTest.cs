﻿namespace BookLibraryManagementTest.DataMapper
{
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class IReaderDataServicesTest
    {
        [TestMethod]
        public void AddReaderTest()
        {
            Reader reader = new Mock<Reader>().Object;

            Mock<IReaderDataServices> mock = new Mock<IReaderDataServices>();
            mock.Setup(m => m.AddReader(reader));

            IReaderDataServices obj = mock.Object;
            obj.AddReader(reader);

            mock.Verify(o => o.AddReader(reader), Times.Once());
        }

        [TestMethod]
        public void DeleteReaderTest()
        {
            Reader reader = new Mock<Reader>().Object;

            Mock<IReaderDataServices> mock = new Mock<IReaderDataServices>();
            mock.Setup(m => m.DeleteReader(reader));

            IReaderDataServices obj = mock.Object;
            obj.DeleteReader(reader);

            mock.Verify(o => o.DeleteReader(reader), Times.Once());
        }

        [TestMethod]
        public void UpdateReaderTest()
        {
            Reader reader = new Mock<Reader>().Object;

            Mock<IReaderDataServices> mock = new Mock<IReaderDataServices>();
            mock.Setup(m => m.UpdateReader(reader));

            IReaderDataServices obj = mock.Object;
            obj.UpdateReader(reader);

            mock.Verify(o => o.UpdateReader(reader), Times.Once());
        }

        [TestMethod]
        public void GetAllReadersTest()
        {
            Mock<IReaderDataServices> mock = new Mock<IReaderDataServices>();
            mock.Setup(m => m.GetAllReaders());

            IReaderDataServices obj = mock.Object;
            obj.GetAllReaders();

            mock.Verify(o => o.GetAllReaders(), Times.Once());
        }

        [TestMethod]
        public void GetReaderByIdTest()
        {
            Mock<IReaderDataServices> mock = new Mock<IReaderDataServices>();
            mock.Setup(m => m.GetReaderById(1));

            IReaderDataServices obj = mock.Object;
            obj.GetReaderById(1);

            mock.Verify(o => o.GetReaderById(1), Times.Once());
        }
    }
}
