﻿namespace BookLibraryManagementTest.DataMapper
{
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class IPublishingHouseDataServicesTest
    {
        [TestMethod]
        public void AddPublishingHouseTest()
        {
            PublishingHouse publishingHouse = new Mock<PublishingHouse>().Object;

            Mock<IPublishingHouseDataServices> mock = new Mock<IPublishingHouseDataServices>();
            mock.Setup(m => m.AddPublishingHouse(publishingHouse));

            IPublishingHouseDataServices obj = mock.Object;
            obj.AddPublishingHouse(publishingHouse);

            mock.Verify(o => o.AddPublishingHouse(publishingHouse), Times.Once());
        }

        [TestMethod]
        public void DeletePublishingHouseTest()
        {
            PublishingHouse publishingHouse = new Mock<PublishingHouse>().Object;

            Mock<IPublishingHouseDataServices> mock = new Mock<IPublishingHouseDataServices>();
            mock.Setup(m => m.DeletePublishingHouse(publishingHouse));

            IPublishingHouseDataServices obj = mock.Object;
            obj.DeletePublishingHouse(publishingHouse);

            mock.Verify(o => o.DeletePublishingHouse(publishingHouse), Times.Once());
        }

        [TestMethod]
        public void UpdatePublishingHouseTest()
        {
            PublishingHouse publishingHouse = new Mock<PublishingHouse>().Object;

            Mock<IPublishingHouseDataServices> mock = new Mock<IPublishingHouseDataServices>();
            mock.Setup(m => m.UpdatePublishingHouse(publishingHouse));

            IPublishingHouseDataServices obj = mock.Object;
            obj.UpdatePublishingHouse(publishingHouse);

            mock.Verify(o => o.UpdatePublishingHouse(publishingHouse), Times.Once());
        }

        [TestMethod]
        public void GetAllPublishingHousesTest()
        {
            Mock<IPublishingHouseDataServices> mock = new Mock<IPublishingHouseDataServices>();
            mock.Setup(m => m.GetAllPublishingHouses());

            IPublishingHouseDataServices obj = mock.Object;
            obj.GetAllPublishingHouses();

            mock.Verify(o => o.GetAllPublishingHouses(), Times.Once());
        }

        [TestMethod]
        public void GetPublishingHouseByIdTest()
        {
            Mock<IPublishingHouseDataServices> mock = new Mock<IPublishingHouseDataServices>();
            mock.Setup(m => m.GetPublishingHouseById(1));

            IPublishingHouseDataServices obj = mock.Object;
            obj.GetPublishingHouseById(1);

            mock.Verify(o => o.GetPublishingHouseById(1), Times.Once());
        }
    }
}
