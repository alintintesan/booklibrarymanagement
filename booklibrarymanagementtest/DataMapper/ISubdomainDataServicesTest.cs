﻿namespace BookLibraryManagementTest.DataMapper
{
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class ISubdomainDataServicesTest
    {
        [TestMethod]
        public void AddSubdomainTest()
        {
            Subdomain subdomain = new Mock<Subdomain>().Object;

            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.AddSubdomain(subdomain));

            ISubdomainDataServices obj = mock.Object;
            obj.AddSubdomain(subdomain);

            mock.Verify(o => o.AddSubdomain(subdomain), Times.Once());
        }

        [TestMethod]
        public void DeleteSubdomainTest()
        {
            Subdomain subdomain = new Mock<Subdomain>().Object;

            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.DeleteSubdomain(subdomain));

            ISubdomainDataServices obj = mock.Object;
            obj.DeleteSubdomain(subdomain);

            mock.Verify(o => o.DeleteSubdomain(subdomain), Times.Once());
        }

        [TestMethod]
        public void UpdateSubdomainTest()
        {
            Subdomain subdomain = new Mock<Subdomain>().Object;

            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.UpdateSubdomain(subdomain));

            ISubdomainDataServices obj = mock.Object;
            obj.UpdateSubdomain(subdomain);

            mock.Verify(o => o.UpdateSubdomain(subdomain), Times.Once());
        }

        [TestMethod]
        public void GetAllSubdomainsTest()
        {
            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.GetAllSubdomains());

            ISubdomainDataServices obj = mock.Object;
            obj.GetAllSubdomains();

            mock.Verify(o => o.GetAllSubdomains(), Times.Once());
        }

        [TestMethod]
        public void GetSubdomainByIdTest()
        {
            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.GetSubdomainById(1));

            ISubdomainDataServices obj = mock.Object;
            obj.GetSubdomainById(1);

            mock.Verify(o => o.GetSubdomainById(1), Times.Once());
        }
    }
}
