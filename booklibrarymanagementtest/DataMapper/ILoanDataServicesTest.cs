﻿namespace BookLibraryManagementTest.DataMapper
{
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class ILoanDataServicesTest
    {
        [TestMethod]
        public void AddLoanTest()
        {
            Loan loan = new Mock<Loan>().Object;

            Mock<ILoanDataServices> mock = new Mock<ILoanDataServices>();
            mock.Setup(m => m.AddLoan(loan));

            ILoanDataServices obj = mock.Object;
            obj.AddLoan(loan);

            mock.Verify(o => o.AddLoan(loan), Times.Once());
        }

        [TestMethod]
        public void DeleteLoanTest()
        {
            Loan loan = new Mock<Loan>().Object;

            Mock<ILoanDataServices> mock = new Mock<ILoanDataServices>();
            mock.Setup(m => m.DeleteLoan(loan));

            ILoanDataServices obj = mock.Object;
            obj.DeleteLoan(loan);

            mock.Verify(o => o.DeleteLoan(loan), Times.Once());
        }

        [TestMethod]
        public void UpdateLoanTest()
        {
            Loan loan = new Mock<Loan>().Object;

            Mock<ILoanDataServices> mock = new Mock<ILoanDataServices>();
            mock.Setup(m => m.UpdateLoan(loan));

            ILoanDataServices obj = mock.Object;
            obj.UpdateLoan(loan);

            mock.Verify(o => o.UpdateLoan(loan), Times.Once());
        }

        [TestMethod]
        public void GetAllLoansTest()
        {
            Mock<ILoanDataServices> mock = new Mock<ILoanDataServices>();
            mock.Setup(m => m.GetAllLoans());

            ILoanDataServices obj = mock.Object;
            obj.GetAllLoans();

            mock.Verify(o => o.GetAllLoans(), Times.Once());
        }

        [TestMethod]
        public void GetLoanByIdTest()
        {
            Mock<ILoanDataServices> mock = new Mock<ILoanDataServices>();
            mock.Setup(m => m.GetLoanById(1));

            ILoanDataServices obj = mock.Object;
            obj.GetLoanById(1);

            mock.Verify(o => o.GetLoanById(1), Times.Once());
        }
    }
}
