﻿namespace BookLibraryManagementTest.Services
{
    using System;
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using BookLibraryManagement.Services;
    using BookLibraryManagement.Services.ServicesImplementation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class DomainServicesTest
    {
        [TestMethod]
        public void TestAddDomainWithValidData()
        {
            Domain domain = new Domain()
            {
                Id = 10,
                Name = "Domain"
            };

            IDomainServices domainServices = new DomainServices();
            bool result = domainServices.AddDomain(domain);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestAddDomainWithInvalidData()
        {
            Domain domain = new Domain();

            IDomainServices readerServices = new DomainServices();
            bool result = readerServices.AddDomain(domain);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestDeleteDomainWithValidData()
        {
            Domain domain = new Domain()
            {
                Id = 10,
                Name = "Domain"
            };

            IDomainServices domainServices = new DomainServices();
            Mock<IDomainDataServices> mock = new Mock<IDomainDataServices>();
            mock.Setup(m => m.DeleteDomain(domain));

            DomainServices.DataServices = mock.Object;
            bool result = domainServices.DeleteDomain(domain);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestDeleteDomainWithInvalidData()
        {
            Domain domain = new Domain();

            IDomainServices domainServices = new DomainServices();
            bool result = domainServices.DeleteDomain(domain);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestUpdateReaderWithValidData()
        {
            Domain domain = new Domain()
            {
                Id = 10,
                Name = "Domain"
            };

            IDomainServices domainServices = new DomainServices();
            bool result = domainServices.UpdateDomain(domain);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestUpdateDomainWithInvalidData()
        {
            Domain domain = new Domain();

            IDomainServices domainServices = new DomainServices();
            bool result = domainServices.UpdateDomain(domain);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestGetListOfDomains()
        {
            IDomainServices domainServices = new DomainServices();
            Mock<IDomainDataServices> mock = new Mock<IDomainDataServices>();
            mock.Setup(m => m.GetAllDomains()).Returns(
                new List<Domain>()
                {
                    new Domain()
                    {
                        Id = 10,
                        Name = "Domain"
                    }
                });

            DomainServices.DataServices = mock.Object;
            var result = domainServices.GetListOfDomains();

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as List<Domain>).Count, 1);
        }

        [TestMethod]
        public void TestGetDomainById()
        {
            IDomainServices domainServices = new DomainServices();
            Mock<IDomainDataServices> mock = new Mock<IDomainDataServices>();
            mock.Setup(m => m.GetDomainById(1)).Returns(
                    new Domain()
                    {
                        Id = 10,
                        Name = "Domain"
                    });

            DomainServices.DataServices = mock.Object;
            var result = domainServices.GetDomainById(1);

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as Domain).Id, 10);
        }

        [TestMethod]
        public void TestGetDomainByIdWithInvalidId()
        {
            IDomainServices domainServices = new DomainServices();
            Mock<IDomainDataServices> mock = new Mock<IDomainDataServices>();
            mock.Setup(m => m.GetDomainById(10)).Returns(
                    new Domain()
                    {
                        Id = 10,
                        Name = "Domain"
                    });

            DomainServices.DataServices = mock.Object;
            var result = domainServices.GetDomainById(1);

            Assert.AreEqual(result, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddNullDomain()
        {
            Domain domain = null;

            IDomainServices domainServices = new DomainServices();
            bool result = domainServices.AddDomain(domain);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDeleteNullDomain()
        {
            Domain domain = null;

            IDomainServices domainServices = new DomainServices();
            bool result = domainServices.DeleteDomain(domain);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestUpdateNullDomain()
        {
            Domain domain = null;

            IDomainServices domainServices = new DomainServices();
            bool result = domainServices.UpdateDomain(domain);
        }
    }
}
