﻿namespace BookLibraryManagementTest.Services
{
    using System;
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using BookLibraryManagement.Services;
    using BookLibraryManagement.Services.ServicesImplementation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class EditionServicesTest
    {
        [TestMethod]
        public void TestAddEditionWithValidData()
        {
            Edition edition = new Edition()
            {
                Id = 1,
                NumberOfPages = 100,
                Type = "Type",
                PublishingHouse = new PublishingHouse()
                {
                    Id = 1,
                    Name = "PublishingHouse"
                }
            };

            IEditionServices editionServices = new EditionServices();
            bool result = editionServices.AddEdition(edition);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestAddEditionWithInvalidData()
        {
            Edition edition = new Edition();

            IEditionServices readerServices = new EditionServices();
            bool result = readerServices.AddEdition(edition);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestDeleteEditionWithValidData()
        {
            Edition edition = new Edition()
            {
                Id = 1,
                NumberOfPages = 100,
                Type = "Type",
                PublishingHouse = new PublishingHouse()
                {
                    Id = 1,
                    Name = "PublishingHouse"
                }
            };

            IEditionServices editionServices = new EditionServices();
            Mock<IEditionDataServices> mock = new Mock<IEditionDataServices>();
            mock.Setup(m => m.DeleteEdition(edition));

            EditionServices.DataServices = mock.Object;
            bool result = editionServices.DeleteEdition(edition);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestDeleteEditionWithInvalidData()
        {
            Edition edition = new Edition();

            IEditionServices editionServices = new EditionServices();
            bool result = editionServices.DeleteEdition(edition);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestUpdateReaderWithValidData()
        {
            Edition edition = new Edition()
            {
                Id = 1,
                NumberOfPages = 100,
                Type = "Type",
                PublishingHouse = new PublishingHouse()
                {
                    Id = 1,
                    Name = "PublishingHouse"
                }
            };

            IEditionServices editionServices = new EditionServices();
            bool result = editionServices.UpdateEdition(edition);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestUpdateEditionWithInvalidData()
        {
            Edition edition = new Edition();

            IEditionServices editionServices = new EditionServices();
            bool result = editionServices.UpdateEdition(edition);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestGetListOfEditions()
        {
            IEditionServices editionServices = new EditionServices();
            Mock<IEditionDataServices> mock = new Mock<IEditionDataServices>();
            mock.Setup(m => m.GetAllEditions()).Returns(
                new List<Edition>()
                {
                    new Edition()
                    {
                        Id = 1,
                        NumberOfPages = 100,
                        Type = "Type",
                        PublishingHouse = new PublishingHouse()
                        {
                            Id = 1,
                            Name = "PublishingHouse"
                        }
                    }
                });

            EditionServices.DataServices = mock.Object;
            var result = editionServices.GetListOfEditions();

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as List<Edition>).Count, 1);
        }

        [TestMethod]
        public void TestGetEditionById()
        {
            IEditionServices editionServices = new EditionServices();
            Mock<IEditionDataServices> mock = new Mock<IEditionDataServices>();
            mock.Setup(m => m.GetEditionById(1)).Returns(
                    new Edition()
                    {
                        Id = 1,
                        NumberOfPages = 100,
                        Type = "Type",
                        PublishingHouse = new PublishingHouse()
                        {
                            Id = 1,
                            Name = "PublishingHouse"
                        }
                    });

            EditionServices.DataServices = mock.Object;
            var result = editionServices.GetEditionById(1);

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as Edition).Id, 1);
        }

        [TestMethod]
        public void TestGetEditionByIdWithInvalidId()
        {
            IEditionServices editionServices = new EditionServices();
            Mock<IEditionDataServices> mock = new Mock<IEditionDataServices>();
            mock.Setup(m => m.GetEditionById(10)).Returns(
                    new Edition()
                    {
                        Id = 10,
                        NumberOfPages = 100,
                        Type = "Type",
                        PublishingHouse = new PublishingHouse()
                        {
                            Id = 1,
                            Name = "PublishingHouse"
                        }
                    });

            EditionServices.DataServices = mock.Object;
            var result = editionServices.GetEditionById(1);

            Assert.AreEqual(result, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddNullEdition()
        {
            Edition edition = null;

            IEditionServices editionServices = new EditionServices();
            bool result = editionServices.AddEdition(edition);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDeleteNullEdition()
        {
            Edition edition = null;

            IEditionServices editionServices = new EditionServices();
            bool result = editionServices.DeleteEdition(edition);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestUpdateNullEdition()
        {
            Edition edition = null;

            IEditionServices editionServices = new EditionServices();
            bool result = editionServices.UpdateEdition(edition);
        }
    }
}
