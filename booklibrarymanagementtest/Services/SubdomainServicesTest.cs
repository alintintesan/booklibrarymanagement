﻿namespace BookLibraryManagementTest.Services
{
    using System;
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using BookLibraryManagement.Services;
    using BookLibraryManagement.Services.ServicesImplementation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class SubdomainServicesTest
    {
        [TestMethod]
        public void TestAddSubdomainWithValidData()
        {
            Subdomain subdomain = new Subdomain()
            {
                Id = 1,
                Name = "Subdomain"
            };

            ISubdomainServices subdomainServices = new SubdomainServices();
            bool result = subdomainServices.AddSubdomain(subdomain);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestAddSubdomainWithInvalidData()
        {
            Subdomain subdomain = new Subdomain();

            ISubdomainServices subdomainServices = new SubdomainServices();
            bool result = subdomainServices.AddSubdomain(subdomain);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestDeleteSubdomainWithValidData()
        {
            Subdomain subdomain = new Subdomain()
            {
                Id = 1,
                Name = "Subdomain"
            };

            ISubdomainServices subdomainServices = new SubdomainServices();
            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.DeleteSubdomain(subdomain));

            SubdomainServices.DataServices = mock.Object;
            bool result = subdomainServices.DeleteSubdomain(subdomain);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestDeleteSubdomainWithInvalidData()
        {
            Subdomain subdomain = new Subdomain();

            ISubdomainServices subdomainServices = new SubdomainServices();
            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();

            mock.Setup(m => m.DeleteSubdomain(subdomain));
            SubdomainServices.DataServices = mock.Object;
            bool result = subdomainServices.DeleteSubdomain(subdomain);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestUpdateSubdomainWithValidData()
        {
            Subdomain subdomain = new Subdomain()
            {
                Id = 1,
                Name = "Subdomain"
            };

            ISubdomainServices subdomainServices = new SubdomainServices();
            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.UpdateSubdomain(subdomain));

            SubdomainServices.DataServices = mock.Object;
            bool result = subdomainServices.UpdateSubdomain(subdomain);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestUpdateSubdomainWithInvalidData()
        {
            Subdomain subdomain = new Subdomain();

            ISubdomainServices subdomainServices = new SubdomainServices();
            bool result = subdomainServices.UpdateSubdomain(subdomain);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestGetListOfSubdomains()
        {
            ISubdomainServices subdomainServices = new SubdomainServices();
            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.GetAllSubdomains()).Returns(
                new List<Subdomain>()
                {
                    new Subdomain()
                    {
                        Id = 1,
                        Name = "Subdomain"
                    }
                });

            SubdomainServices.DataServices = mock.Object;
            var result = subdomainServices.GetListOfSubdomains();

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as List<Subdomain>).Count, 1);
        }

        [TestMethod]
        public void TestGetSubdomainById()
        {
            ISubdomainServices subdomainServices = new SubdomainServices();
            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.GetSubdomainById(1)).Returns(
                    new Subdomain()
                    {
                        Id = 1,
                        Name = "Subdomain"
                    });

            SubdomainServices.DataServices = mock.Object;
            var result = subdomainServices.GetSubdomainById(1);

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as Subdomain).Id, 1);
        }

        [TestMethod]
        public void TestGetSubdomainByIdWithInvalidId()
        {
            ISubdomainServices subdomainServices = new SubdomainServices();
            Mock<ISubdomainDataServices> mock = new Mock<ISubdomainDataServices>();
            mock.Setup(m => m.GetSubdomainById(10)).Returns(
                    new Subdomain()
                    {
                        Id = 10,
                        Name = "Subdomain"
                    });

            SubdomainServices.DataServices = mock.Object;
            var result = subdomainServices.GetSubdomainById(1);

            Assert.AreEqual(result, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddNullSubdomain()
        {
            Subdomain subdomain = null;

            ISubdomainServices subdomainServices = new SubdomainServices();
            bool result = subdomainServices.AddSubdomain(subdomain);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDeleteNullSubdomain()
        {
            Subdomain subdomain = null;

            ISubdomainServices subdomainServices = new SubdomainServices();
            bool result = subdomainServices.DeleteSubdomain(subdomain);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestUpdateNullSubdomain()
        {
            Subdomain subdomain = null;

            ISubdomainServices subdomainServices = new SubdomainServices();
            bool result = subdomainServices.UpdateSubdomain(subdomain);
        }
    }
}
