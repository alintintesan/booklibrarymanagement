﻿namespace BookLibraryManagementTest.Services
{
    using System;
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using BookLibraryManagement.Services;
    using BookLibraryManagement.Services.ServicesImplementation;
    using FluentValidation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class BookServicesTest
    {
        [TestMethod]
        public void TestAddBookWithValidData()
        {
            Book book = new Book()
            {
                Id = 10,
                Name = "Book",
                IsForReadingRoomOnly = true
            };

            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            bool result = bookServices.AddBook(book);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestAddBookWithInvalidData()
        {
            Book book = new Book();

            IBookServices readerServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            bool result = readerServices.AddBook(book);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestDeleteBookWithValidData()
        {
            Book book = new Book()
            {
                Id = 10,
                Name = "Book", 
                IsForReadingRoomOnly = true
            };

            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            Mock<IBookDataServices> mock = new Mock<IBookDataServices>();
            mock.Setup(m => m.DeleteBook(book));

            BookServices.DataServices = mock.Object;
            bool result = bookServices.DeleteBook(book);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestDeleteBookWithInvalidData()
        {
            Book book = new Book();

            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            bool result = bookServices.DeleteBook(book);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestUpdateReaderWithValidData()
        {
            Book book = new Book()
            {
                Id = 10,
                Name = "Book",
                IsForReadingRoomOnly = true
            };

            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            bool result = bookServices.UpdateBook(book);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestUpdateBookWithInvalidData()
        {
            Book book = new Book();

            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            bool result = bookServices.UpdateBook(book);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestGetListOfBooks()
        {
            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            Mock<IBookDataServices> mock = new Mock<IBookDataServices>();
            mock.Setup(m => m.GetAllBooks()).Returns(
                new List<Book>()
                {
                    new Book()
                    {
                        Id = 10,
                        Name = "Book"
                    }
                });

            BookServices.DataServices = mock.Object;
            var result = bookServices.GetListOfBooks();

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as List<Book>).Count, 1);
        }

        [TestMethod]
        public void TestGetBookById()
        {
            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            Mock<IBookDataServices> mock = new Mock<IBookDataServices>();
            mock.Setup(m => m.GetBookById(1)).Returns(
                    new Book()
                    {
                        Id = 10,
                        Name = "Book"
                    });

            BookServices.DataServices = mock.Object;
            var result = bookServices.GetBookById(1);

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as Book).Id, 10);
        }

        [TestMethod]
        public void TestGetBookByIdWithInvalidId()
        {
            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            Mock<IBookDataServices> mock = new Mock<IBookDataServices>();
            mock.Setup(m => m.GetBookById(10)).Returns(
                    new Book()
                    {
                        Id = 10,
                        Name = "Book"
                    });

            BookServices.DataServices = mock.Object;
            var result = bookServices.GetBookById(1);

            Assert.AreEqual(result, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddNullBook()
        {
            Book book = null;

            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            bool result = bookServices.AddBook(book);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDeleteNullBook()
        {
            Book book = null;

            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            bool result = bookServices.DeleteBook(book);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestUpdateNullBook()
        {
            Book book = null;

            IBookServices bookServices = new BookServices();
            BookServices.Validators = new List<AbstractValidator<Book>>() { new BookValidator() };
            bool result = bookServices.UpdateBook(book);
        }
    }
}
