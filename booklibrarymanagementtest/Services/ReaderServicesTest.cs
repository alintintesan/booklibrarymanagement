﻿namespace BookLibraryManagementTest.Services
{
    using System;
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using BookLibraryManagement.Services;
    using BookLibraryManagement.Services.ServicesImplementation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class ReaderServicesTest
    {
        [TestMethod]
        public void TestAddReaderWithValidData()
        {
            Reader reader = new Reader()
            {
                Id = 1,
                Address = "abcdef",
                Email = "test@example.com",
                FirstName = "FirstName",
                LastName = "LastName",
                IsEmployee = false,
                PhoneNumber = "0712345678"
            };

            IReaderServices readerServices = new ReaderServices();
            bool result = readerServices.AddReader(reader);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestAddReaderWithInvalidData()
        {
            Reader reader = new Reader();

            IReaderServices readerServices = new ReaderServices();
            bool result = readerServices.AddReader(reader);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestDeleteReaderWithValidData()
        {
            Reader reader = new Reader()
            {
                Id = 1,
                Address = "abcdef",
                Email = "test@example.com",
                FirstName = "FirstName",
                LastName = "LastName",
                IsEmployee = false,
                PhoneNumber = "0712345678"
            };

            IReaderServices readerServices = new ReaderServices();
            Mock<IReaderDataServices> mock = new Mock<IReaderDataServices>();

            mock.Setup(m => m.DeleteReader(reader));
            ReaderServices.DataServices = mock.Object;

            bool result = readerServices.DeleteReader(reader);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestDeleteReaderWithInvalidData()
        {
            Reader reader = new Reader();

            IReaderServices readerServices = new ReaderServices();
            bool result = readerServices.DeleteReader(reader);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestUpdateReaderWithValidData()
        {
            Reader reader = new Reader()
            {
                Id = 1,
                Address = "abcdef",
                Email = "test@example.com",
                FirstName = "FirstName",
                LastName = "LastName",
                IsEmployee = false,
                PhoneNumber = "0712345678"
            };

            IReaderServices readerServices = new ReaderServices();
            bool result = readerServices.UpdateReader(reader);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestUpdateReaderWithInvalidData()
        {
            Reader reader = new Reader();

            IReaderServices readerServices = new ReaderServices();
            bool result = readerServices.UpdateReader(reader);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestGetListOfReaders()
        {
            IReaderServices readerServices = new ReaderServices();
            Mock<IReaderDataServices> mock = new Mock<IReaderDataServices>();
            mock.Setup(m => m.GetAllReaders()).Returns(
                new List<Reader>()
                {
                    new Reader()
                    {
                        Id = 1,
                        Address = "abcdef",
                        Email = "test@example.com",
                        FirstName = "FirstName",
                        LastName = "LastName",
                        IsEmployee = false,
                        PhoneNumber = "0712345678"
                    }
                });

            ReaderServices.DataServices = mock.Object;
            var result = readerServices.GetListOfReaders();

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as List<Reader>).Count, 1);
        }

        [TestMethod]
        public void TestGetReaderById()
        {
            IReaderServices readerServices = new ReaderServices();
            Mock<IReaderDataServices> mock = new Mock<IReaderDataServices>();
            mock.Setup(m => m.GetReaderById(1)).Returns(
                    new Reader()
                    {
                        Id = 1,
                        Address = "abcdef",
                        Email = "test@example.com",
                        FirstName = "FirstName",
                        LastName = "LastName",
                        IsEmployee = false,
                        PhoneNumber = "0712345678"
                    });

            ReaderServices.DataServices = mock.Object;
            var result = readerServices.GetReaderById(1);

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as Reader).Id, 1);
        }

        [TestMethod]
        public void TestGetReaderByIdWithInvalidId()
        {
            IReaderServices readerServices = new ReaderServices();
            Mock<IReaderDataServices> mock = new Mock<IReaderDataServices>();
            mock.Setup(m => m.GetReaderById(10)).Returns(
                    new Reader()
                    {
                        Id = 10,
                        Address = "abcdef",
                        Email = "test@example.com",
                        FirstName = "FirstName",
                        LastName = "LastName",
                        IsEmployee = false,
                        PhoneNumber = "0712345678"
                    });

            ReaderServices.DataServices = mock.Object;
            var result = readerServices.GetReaderById(1);

            Assert.AreEqual(result, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddNullReader()
        {
            Reader reader = null;

            IReaderServices readerServices = new ReaderServices();
            bool result = readerServices.AddReader(reader);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDeleteNullReader()
        {
            Reader reader = null;

            IReaderServices readerServices = new ReaderServices();
            bool result = readerServices.DeleteReader(reader);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestUpdateNullReader()
        {
            Reader reader = null;

            IReaderServices readerServices = new ReaderServices();
            bool result = readerServices.UpdateReader(reader);
        }
    }
}
