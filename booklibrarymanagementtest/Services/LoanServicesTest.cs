﻿namespace BookLibraryManagementTest.Services
{
    using System;
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using BookLibraryManagement.Services;
    using BookLibraryManagement.Services.ServicesImplementation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class LoanServicesTest
    {
        [TestMethod]
        public void TestAddLoanWithValidData()
        {
            Loan loan = new Loan()
            {
                Id = 1,
                Reader = new Reader()
                {
                    Id = 1,
                    Address = "abcdef",
                    Email = "test@example.com",
                    FirstName = "FirstName",
                    LastName = "LastName",
                    IsEmployee = false,
                    PhoneNumber = "0712345678"
                }
        };

            ILoanServices loanServices = new LoanServices();
            bool result = loanServices.AddLoan(loan);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestAddLoanWithInvalidData()
        {
            Loan loan = new Loan();

            ILoanServices readerServices = new LoanServices();
            bool result = readerServices.AddLoan(loan);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestDeleteLoanWithValidData()
        {
            Loan loan = new Loan()
            {
                Id = 1,
                Reader = new Reader()
                {
                    Id = 1,
                    Address = "abcdef",
                    Email = "test@example.com",
                    FirstName = "FirstName",
                    LastName = "LastName",
                    IsEmployee = false,
                    PhoneNumber = "0712345678"
                }
            };

            ILoanServices loanServices = new LoanServices();
            Mock<ILoanDataServices> mock = new Mock<ILoanDataServices>();
            mock.Setup(m => m.DeleteLoan(loan));

            LoanServices.DataServices = mock.Object;
            bool result = loanServices.DeleteLoan(loan);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestDeleteLoanWithInvalidData()
        {
            Loan loan = new Loan();

            ILoanServices loanServices = new LoanServices();
            bool result = loanServices.DeleteLoan(loan);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestUpdateReaderWithValidData()
        {
            Loan loan = new Loan()
            {
                Id = 1,
                Reader = new Reader()
                {
                    Id = 1,
                    Address = "abcdef",
                    Email = "test@example.com",
                    FirstName = "FirstName",
                    LastName = "LastName",
                    IsEmployee = false,
                    PhoneNumber = "0712345678"
                }
            };

            ILoanServices loanServices = new LoanServices();
            bool result = loanServices.UpdateLoan(loan);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestUpdateLoanWithInvalidData()
        {
            Loan loan = new Loan();

            ILoanServices loanServices = new LoanServices();
            bool result = loanServices.UpdateLoan(loan);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestGetListOfLoans()
        {
            ILoanServices loanServices = new LoanServices();
            Mock<ILoanDataServices> mock = new Mock<ILoanDataServices>();
            mock.Setup(m => m.GetAllLoans()).Returns(
                new List<Loan>()
                {
                    new Loan()
                    {
                        Id = 1,
                        Reader = new Reader()
                        {
                            Id = 1,
                            Address = "abcdef",
                            Email = "test@example.com",
                            FirstName = "FirstName",
                            LastName = "LastName",
                            IsEmployee = false,
                            PhoneNumber = "0712345678"
                        }
                    }
                });

            LoanServices.DataServices = mock.Object;
            var result = loanServices.GetListOfLoans();

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as List<Loan>).Count, 1);
        }

        [TestMethod]
        public void TestGetLoanById()
        {
            ILoanServices loanServices = new LoanServices();
            Mock<ILoanDataServices> mock = new Mock<ILoanDataServices>();
            mock.Setup(m => m.GetLoanById(1)).Returns(
                    new Loan()
                    {
                        Id = 1,
                        Reader = new Reader()
                        {
                            Id = 1,
                            Address = "abcdef",
                            Email = "test@example.com",
                            FirstName = "FirstName",
                            LastName = "LastName",
                            IsEmployee = false,
                            PhoneNumber = "0712345678"
                        }
                    });

            LoanServices.DataServices = mock.Object;
            var result = loanServices.GetLoanById(1);

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as Loan).Id, 1);
        }

        [TestMethod]
        public void TestGetLoanByIdWithInvalidId()
        {
            ILoanServices loanServices = new LoanServices();
            Mock<ILoanDataServices> mock = new Mock<ILoanDataServices>();
            mock.Setup(m => m.GetLoanById(10)).Returns(
                    new Loan()
                    {
                        Id = 10,
                        Reader = new Reader()
                        {
                            Id = 1,
                            Address = "abcdef",
                            Email = "test@example.com",
                            FirstName = "FirstName",
                            LastName = "LastName",
                            IsEmployee = false,
                            PhoneNumber = "0712345678"
                        }
                    });

            LoanServices.DataServices = mock.Object;
            var result = loanServices.GetLoanById(1);

            Assert.AreEqual(result, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddNullLoan()
        {
            Loan loan = null;

            ILoanServices loanServices = new LoanServices();
            bool result = loanServices.AddLoan(loan);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDeleteNullLoan()
        {
            Loan loan = null;

            ILoanServices loanServices = new LoanServices();
            bool result = loanServices.DeleteLoan(loan);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestUpdateNullLoan()
        {
            Loan loan = null;

            ILoanServices loanServices = new LoanServices();
            bool result = loanServices.UpdateLoan(loan);
        }
    }
}
