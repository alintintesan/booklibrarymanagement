﻿namespace BookLibraryManagementTest.Services
{
    using System;
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using BookLibraryManagement.Services;
    using BookLibraryManagement.Services.ServicesImplementation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class PublishingHouseServiceTest
    {
        [TestMethod]
        public void TestAddPublishingHouseWithValidData()
        {
            PublishingHouse publishingHouse = new PublishingHouse()
            {
                Id = 1,
                Name = "PublishingHouse"
            };

            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            bool result = publishingHouseServices.AddPublishingHouse(publishingHouse);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestAddPublishingHouseWithInvalidData()
        {
            PublishingHouse publishingHouse = new PublishingHouse();

            IPublishingHouseServices readerServices = new PublishingHouseServices();
            bool result = readerServices.AddPublishingHouse(publishingHouse);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestDeletePublishingHouseWithValidData()
        {
            PublishingHouse publishingHouse = new PublishingHouse()
            {
                Id = 1,
                Name = "PublishingHouse"
            };

            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            Mock<IPublishingHouseDataServices> mock = new Mock<IPublishingHouseDataServices>();

            mock.Setup(m => m.DeletePublishingHouse(publishingHouse));
            PublishingHouseServices.DataServices = mock.Object;

            bool result = publishingHouseServices.DeletePublishingHouse(publishingHouse);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestDeletePublishingHouseWithInvalidData()
        {
            PublishingHouse publishingHouse = new PublishingHouse();

            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            bool result = publishingHouseServices.DeletePublishingHouse(publishingHouse);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestUpdateReaderWithValidData()
        {
            PublishingHouse publishingHouse = new PublishingHouse()
            {
                Id = 1,
                Name = "PublishingHouse"
            };

            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            bool result = publishingHouseServices.UpdatePublishingHouse(publishingHouse);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestUpdatePublishingHouseWithInvalidData()
        {
            PublishingHouse publishingHouse = new PublishingHouse();

            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            bool result = publishingHouseServices.UpdatePublishingHouse(publishingHouse);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestGetListOfPublishingHouses()
        {
            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            Mock<IPublishingHouseDataServices> mock = new Mock<IPublishingHouseDataServices>();
            mock.Setup(m => m.GetAllPublishingHouses()).Returns(
                new List<PublishingHouse>()
                {
                    new PublishingHouse()
                    {
                        Id = 1,
                        Name = "PublishingHouse"
                    }
                });

            PublishingHouseServices.DataServices = mock.Object;
            var result = publishingHouseServices.GetListOfPublishingHouses();

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as List<PublishingHouse>).Count, 1);
        }

        [TestMethod]
        public void TestGetPublishingHouseById()
        {
            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            Mock<IPublishingHouseDataServices> mock = new Mock<IPublishingHouseDataServices>();
            mock.Setup(m => m.GetPublishingHouseById(1)).Returns(
                    new PublishingHouse()
                    {
                        Id = 1,
                        Name = "PublishingHouse"
                    });

            PublishingHouseServices.DataServices = mock.Object;
            var result = publishingHouseServices.GetPublishingHouseById(1);

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as PublishingHouse).Id, 1);
        }

        [TestMethod]
        public void TestGetPublishingHouseByIdWithInvalidId()
        {
            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            Mock<IPublishingHouseDataServices> mock = new Mock<IPublishingHouseDataServices>();
            mock.Setup(m => m.GetPublishingHouseById(10)).Returns(
                    new PublishingHouse()
                    {
                        Id = 10,
                        Name = "PublishingHouse"
                    });

            PublishingHouseServices.DataServices = mock.Object;
            var result = publishingHouseServices.GetPublishingHouseById(1);

            Assert.AreEqual(result, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddNullPublishingHouse()
        {
            PublishingHouse publishingHouse = null;

            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            bool result = publishingHouseServices.AddPublishingHouse(publishingHouse);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDeleteNullPublishingHouse()
        {
            PublishingHouse publishingHouse = null;

            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            bool result = publishingHouseServices.DeletePublishingHouse(publishingHouse);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestUpdateNullPublishingHouse()
        {
            PublishingHouse publishingHouse = null;

            IPublishingHouseServices publishingHouseServices = new PublishingHouseServices();
            bool result = publishingHouseServices.UpdatePublishingHouse(publishingHouse);
        }
    }
}
