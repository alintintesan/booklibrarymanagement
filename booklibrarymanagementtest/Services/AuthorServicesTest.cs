﻿namespace AuthorLibraryManagementTest.Services
{
    using System;
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using BookLibraryManagement.Services;
    using BookLibraryManagement.Services.ServicesImplementation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class AuthorServicesTest
    {
        [TestMethod]
        public void TestAddAuthorWithValidData()
        {
            Author author = new Author()
            {
                Id = 10,
                Name = "Author"
            };

            IAuthorServices authorServices = new AuthorServices();
            bool result = authorServices.AddAuthor(author);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestAddAuthorWithInvalidData()
        {
            Author author = new Author();

            IAuthorServices readerServices = new AuthorServices();
            bool result = readerServices.AddAuthor(author);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestDeleteAuthorWithValidData()
        {
            Author author = new Author()
            {
                Id = 10,
                Name = "Author"
            };

            IAuthorServices authorServices = new AuthorServices();
            Mock<IAuthorDataServices> mock = new Mock<IAuthorDataServices>();
            mock.Setup(m => m.DeleteAuthor(author));

            AuthorServices.DataServices = mock.Object;
            bool result = authorServices.DeleteAuthor(author);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestDeleteAuthorWithInvalidData()
        {
            Author author = new Author();

            IAuthorServices authorServices = new AuthorServices();
            bool result = authorServices.DeleteAuthor(author);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestUpdateReaderWithValidData()
        {
            Author author = new Author()
            {
                Id = 10,
                Name = "Author"
            };

            IAuthorServices authorServices = new AuthorServices();
            bool result = authorServices.UpdateAuthor(author);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestUpdateAuthorWithInvalidData()
        {
            Author author = new Author();

            IAuthorServices authorServices = new AuthorServices();
            bool result = authorServices.UpdateAuthor(author);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestGetListOfAuthors()
        {
            IAuthorServices authorServices = new AuthorServices();
            Mock<IAuthorDataServices> mock = new Mock<IAuthorDataServices>();
            mock.Setup(m => m.GettAllAuthors()).Returns(
                new List<Author>()
                {
                    new Author()
                    {
                        Id = 10,
                        Name = "Author"
                    }
                });

            AuthorServices.DataServices = mock.Object;
            var result = authorServices.GetListOfAuthors();

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as List<Author>).Count, 1);
        }

        [TestMethod]
        public void TestGetAuthorById()
        {
            IAuthorServices authorServices = new AuthorServices();
            Mock<IAuthorDataServices> mock = new Mock<IAuthorDataServices>();
            mock.Setup(m => m.GetAuthorById(1)).Returns(
                    new Author()
                    {
                        Id = 10,
                        Name = "Author"
                    });

            AuthorServices.DataServices = mock.Object;
            var result = authorServices.GetAuthorById(1);

            Assert.AreNotEqual(result, null);
            Assert.AreEqual((result as Author).Id, 10);
        }

        [TestMethod]
        public void TestGetAuthorByIdWithInvalidId()
        {
            IAuthorServices authorServices = new AuthorServices();
            Mock<IAuthorDataServices> mock = new Mock<IAuthorDataServices>();
            mock.Setup(m => m.GetAuthorById(10)).Returns(
                    new Author()
                    {
                        Id = 10,
                        Name = "Author"
                    });

            AuthorServices.DataServices = mock.Object;
            var result = authorServices.GetAuthorById(1);

            Assert.AreEqual(result, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddNullAuthor()
        {
            Author author = null;

            IAuthorServices authorServices = new AuthorServices();
            bool result = authorServices.AddAuthor(author);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDeleteNullAuthor()
        {
            Author author = null;

            IAuthorServices authorServices = new AuthorServices();
            bool result = authorServices.DeleteAuthor(author);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestUpdateNullAuthor()
        {
            Author author = null;

            IAuthorServices authorServices = new AuthorServices();
            bool result = authorServices.UpdateAuthor(author);
        }
    }
}
