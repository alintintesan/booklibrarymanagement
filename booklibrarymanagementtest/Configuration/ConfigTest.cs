﻿namespace BookLibraryManagementTest.Configuration
{
    using BookLibraryManagement.Configuration;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ConfigTest
    {
        [TestMethod]
        public void ConfigDataTest()
        {
            Assert.AreEqual(Config.DataProvider, "sqlserver");
            Assert.AreEqual(Config.Dom, 5);
            Assert.AreEqual(Config.C, 10);
        }
    }
}
