﻿namespace BookLibraryManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Book_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Books", t => t.Book_Id)
                .Index(t => t.Book_Id);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsForReadingRoomOnly = c.Boolean(nullable: false),
                        Loan_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Loans", t => t.Loan_Id)
                .Index(t => t.Loan_Id);
            
            CreateTable(
                "dbo.Editions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 15),
                        NumberOfPages = c.Int(nullable: false),
                        PublishingHouse_Id = c.Int(nullable: false),
                        Book_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PublishingHouses", t => t.PublishingHouse_Id, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.Book_Id)
                .Index(t => t.PublishingHouse_Id)
                .Index(t => t.Book_Id);
            
            CreateTable(
                "dbo.PublishingHouses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Domains",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Book_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Books", t => t.Book_Id)
                .Index(t => t.Book_Id);
            
            CreateTable(
                "dbo.Subdomains",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Domain_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Domains", t => t.Domain_Id)
                .Index(t => t.Domain_Id);
            
            CreateTable(
                "dbo.Loans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Reader_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Readers", t => t.Reader_Id, cascadeDelete: true)
                .Index(t => t.Reader_Id);
            
            CreateTable(
                "dbo.Readers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        Address = c.String(nullable: false, maxLength: 200),
                        Email = c.String(nullable: false, maxLength: 50),
                        PhoneNumber = c.String(nullable: false, maxLength: 10),
                        IsEmployee = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Loans", "Reader_Id", "dbo.Readers");
            DropForeignKey("dbo.Books", "Loan_Id", "dbo.Loans");
            DropForeignKey("dbo.Domains", "Book_Id", "dbo.Books");
            DropForeignKey("dbo.Subdomains", "Domain_Id", "dbo.Domains");
            DropForeignKey("dbo.Editions", "Book_Id", "dbo.Books");
            DropForeignKey("dbo.Editions", "PublishingHouse_Id", "dbo.PublishingHouses");
            DropForeignKey("dbo.Authors", "Book_Id", "dbo.Books");
            DropIndex("dbo.Loans", new[] { "Reader_Id" });
            DropIndex("dbo.Subdomains", new[] { "Domain_Id" });
            DropIndex("dbo.Domains", new[] { "Book_Id" });
            DropIndex("dbo.Editions", new[] { "Book_Id" });
            DropIndex("dbo.Editions", new[] { "PublishingHouse_Id" });
            DropIndex("dbo.Books", new[] { "Loan_Id" });
            DropIndex("dbo.Authors", new[] { "Book_Id" });
            DropTable("dbo.Readers");
            DropTable("dbo.Loans");
            DropTable("dbo.Subdomains");
            DropTable("dbo.Domains");
            DropTable("dbo.PublishingHouses");
            DropTable("dbo.Editions");
            DropTable("dbo.Books");
            DropTable("dbo.Authors");
        }
    }
}
