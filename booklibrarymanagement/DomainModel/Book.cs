﻿// <copyright file="Book.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DomainModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using BookLibraryManagement.Configuration;
    using FluentValidation;

    /// <summary>Book Model class</summary>
    public class Book
    {
        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        [Key]
        [Required]
        public int Id { get; set; }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        /// <summary>Gets or sets a value indicating whether this instance is for reading room only.</summary>
        /// <value>
        ///   <c>true</c> if this instance is for reading room only; otherwise, <c>false</c>.</value>
        [Required]
        public bool IsForReadingRoomOnly { get; set; }

        /// <summary>Gets or sets the domains.</summary>
        /// <value>The domains.</value>
        public virtual List<Domain> Domains { get; set; }

        /// <summary>Gets or sets the authors.</summary>
        /// <value>The authors.</value>
        public virtual List<Author> Authors { get; set; }

        /// <summary>
        ///   <para></para>
        ///   <para>Gets or sets the editions.
        /// </para>
        /// </summary>
        /// <value>The editions.</value>
        public virtual List<Edition> Editions { get; set; }
    }

    /// <summary>Book validator.</summary>
    /// <seealso cref="FluentValidation.AbstractValidator{BookLibraryManagement.DomainModel.Book}" />
    public class BookValidator : AbstractValidator<Book>
    {
        /// <summary>Initializes a new instance of the <see cref="BookValidator"/> class.</summary>
        public BookValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Name).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.IsForReadingRoomOnly).NotEmpty().WithErrorCode("This field is required.");
        }
    }

    /// <summary>Book complex validator.</summary>
    /// <seealso cref="FluentValidation.AbstractValidator{BookLibraryManagement.DomainModel.Book}" />
    public class BookComplexValidator : AbstractValidator<Book>
    {
        /// <summary>Initializes a new instance of the <see cref="BookComplexValidator"/> class.</summary>
        public BookComplexValidator()
        {
            RuleFor(x => x.Authors).NotEmpty().NotNull();
            RuleFor(x => x.Domains).NotEmpty().NotNull();
            RuleFor(x => x.Authors).Must(HaveAValidLength).WithMessage("Please specify a valid length.");
            RuleFor(x => x.Domains).Must(HaveAValidLength).WithMessage("Please specify a valid length.");
            RuleFor(x => x.Domains).Must(HaveSubdomains).WithMessage("Please specify at least one subdomain for each domain.");
            RuleFor(x => x.Domains).Must(MaximumDomainsPerBook).WithMessage("Please specify less domains.");
            RuleFor(x => x.Domains).Must(NotBeIdenticalToItsSubdomains).WithMessage("Please specify different subdomains for domain.");
        }

        /// <summary>Verifies the length.</summary>
        /// <typeparam name="T">The type.</typeparam>
        /// <param name="data">The data.</param>
        /// <returns>Condition met or not.</returns>
        private bool HaveAValidLength<T>(List<T> data)
        {
            return data.Count >= 1;
        }

        /// <summary>Verifies the subdomains.</summary>
        /// <param name="domains">The domains.</param>
        /// <returns>Condition met or not.</returns>
        private bool HaveSubdomains(List<Domain> domains)
        {
            foreach (var domain in domains)
            {
                if (domain.Subdomains.Count < 1)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>Verifies the number of domains per book.</summary>
        /// <param name="data">The data.</param>
        /// <returns>Condition met or not.</returns>
        private bool MaximumDomainsPerBook(List<Domain> data)
        {
            return data.Count <= Config.Dom;
        }

        /// <summary>Checks the domain to not be identical to its subdomains.</summary>
        /// <param name="domains">The domains.</param>
        /// <returns>Condition met or not.</returns>
        private bool NotBeIdenticalToItsSubdomains(List<Domain> domains)
        {
            foreach (var domain in domains)
            {
                foreach (var subdomain in domain.Subdomains)
                {
                    if (domain.Name.Equals(subdomain.Name))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
