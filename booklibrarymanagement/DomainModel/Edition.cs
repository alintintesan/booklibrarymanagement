﻿// <copyright file="Edition.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DomainModel
{
    using System.ComponentModel.DataAnnotations;
    using FluentValidation;

    /// <summary>Edition Model class</summary>
    public class Edition
    {
        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        [Key]
        [Required]
        public int Id { get; set; }

        /// <summary>Gets or sets the publishing house.</summary>
        /// <value>The publishing house.</value>
        [Required]
        public PublishingHouse PublishingHouse { get; set; }

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        [Required]
        [StringLength(15, MinimumLength = 2)]
        public string Type { get; set; }

        /// <summary>Gets or sets the number of pages.</summary>
        /// <value>The number of pages.</value>
        [Required]
        public int NumberOfPages { get; set; }
    }

    /// <summary>Edition validator.</summary>
    /// <seealso cref="FluentValidation.AbstractValidator{BookLibraryManagement.DomainModel.Edition}" />
    public class EditionValidator : AbstractValidator<Edition>
    {
        /// <summary>Initializes a new instance of the <see cref="EditionValidator"/> class.</summary>
        public EditionValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.PublishingHouse).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Type).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Type).Length(2, 15);
            RuleFor(x => x.NumberOfPages).NotEmpty().WithErrorCode("This field is required.");
        }
    }
}
