﻿// <copyright file="Author.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DomainModel
{
    using System.ComponentModel.DataAnnotations;
    using FluentValidation;

    /// <summary>Author Model class</summary>
    public class Author
    {
        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        [Key]
        [Required]
        public int Id { get; set; }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
    }

    /// <summary>Author validator.</summary>
    /// <seealso cref="FluentValidation.AbstractValidator{BookLibraryManagement.DomainModel.Author}" />
    public class AuthorValidator : AbstractValidator<Author>
    {
        public AuthorValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Name).NotEmpty().WithErrorCode("This field is required.");
        }
    }
}
