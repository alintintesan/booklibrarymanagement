﻿// <copyright file="PublishingHouse.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DomainModel
{
    using System.ComponentModel.DataAnnotations;
    using FluentValidation;

    /// <summary>Publishing House Model class.</summary>
    public class PublishingHouse
    {
        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        [Key]
        [Required]
        public int Id { get; set; }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
    }

    /// <summary>PublishingHouse validator.</summary>
    /// <seealso cref="FluentValidation.AbstractValidator{BookLibraryManagement.DomainModel.PublishingHouse}" />
    public class PublishingHouseValidator : AbstractValidator<PublishingHouse>
    {
        /// <summary>Initializes a new instance of the <see cref="PublishingHouseValidator"/> class.</summary>
        public PublishingHouseValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Name).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Name).Length(2, 50);
        }
    }
}
