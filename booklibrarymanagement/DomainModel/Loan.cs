﻿// <copyright file="Loan.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DomainModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using FluentValidation;

    /// <summary>Loan Model class.</summary>
    public class Loan
    {
        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        [Key]
        [Required]
        public int Id { get; set; }

        /// <summary>Gets or sets the reader.</summary>
        /// <value>The reader.</value>
        [Required]
        public Reader Reader { get; set; }

        /// <summary>Gets or sets the books.</summary>
        /// <value>The books.</value>
        public virtual List<Book> Books { get; set; }
    }

    /// <summary>Loan validator.</summary>
    /// <seealso cref="FluentValidation.AbstractValidator{BookLibraryManagement.DomainModel.Loan}" />
    public class LoanValidator : AbstractValidator<Loan>
    {
        /// <summary>Initializes a new instance of the <see cref="LoanValidator"/> class.</summary>
        public LoanValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Reader).NotEmpty().WithErrorCode("This field is required.");
        }
    }
}
