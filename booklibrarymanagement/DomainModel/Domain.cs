﻿// <copyright file="Domain.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DomainModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using FluentValidation;

    /// <summary>Domain Model class.</summary>
    public class Domain
    {
        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        [Key]
        [Required]
        public int Id { get; set; }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        /// <summary>Gets or sets the subdomains.</summary>
        /// <value>The subdomains.</value>
        public virtual List<Subdomain> Subdomains { get; set; }
    }

    /// <summary>Domain validator.</summary>
    /// <seealso cref="FluentValidation.AbstractValidator{BookLibraryManagement.DomainModel.Domain}" />
    public class DomainValidator : AbstractValidator<Domain>
    {
        /// <summary>Initializes a new instance of the <see cref="DomainValidator"/> class.</summary>
        public DomainValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Name).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Name).Length(2, 50);
        }
    }
}
