﻿// <copyright file="Reader.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DomainModel
{
    using System.ComponentModel.DataAnnotations;
    using FluentValidation;

    /// <summary>Reader Model class</summary>
    public class Reader
    {
        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        [Key]
        [Required]
        public int Id { get; set; }

        /// <summary>Gets or sets the first name.</summary>
        /// <value>The first name.</value>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string FirstName { get; set; }

        /// <summary>Gets or sets the last name.</summary>
        /// <value>The last name.</value>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string LastName { get; set; }

        /// <summary>Gets or sets the address.</summary>
        /// <value>The address.</value>
        [Required]
        [StringLength(200, MinimumLength = 2)]
        public string Address { get; set; }

        /// <summary>Gets or sets the email.</summary>
        /// <value>The email.</value>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Email { get; set; }

        /// <summary>Gets or sets the phone number.</summary>
        /// <value>The phone number.</value>
        [Required]
        [StringLength(10, MinimumLength = 2)]
        public string PhoneNumber { get; set; }

        /// <summary>Gets or sets a value indicating whether this instance is employee.</summary>
        /// <value>
        /// <c>true</c> if this instance is employee; otherwise, <c>false</c>.</value>
        [Required]
        public bool IsEmployee { get; set; } = false;
    }

    /// <summary>Reader validator.</summary>
    /// <seealso cref="FluentValidation.AbstractValidator{BookLibraryManagement.DomainModel.Reader}" />
    public class ReaderValidator : AbstractValidator<Reader>
    {
        /// <summary>Initializes a new instance of the <see cref="ReaderValidator"/> class.</summary>
        public ReaderValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.FirstName).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.FirstName).Length(2, 50);
            RuleFor(x => x.LastName).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.LastName).Length(2, 50);
            RuleFor(x => x.Address).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.LastName).Length(2, 200);
            RuleFor(x => x.Email).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.Email).Length(2, 50);
            RuleFor(x => x.PhoneNumber).NotEmpty().WithErrorCode("This field is required.");
            RuleFor(x => x.PhoneNumber).Length(2, 10);
        }
    }
}
