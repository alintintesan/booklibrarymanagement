﻿// <copyright file="Config.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Configuration
{
    /// <summary>
    /// Config class.
    /// </summary>
    public class Config
    {
        /// <summary>The data provider.</summary>
        public static readonly string DataProvider = "sqlserver";

        /// <summary>The maximum domains per book.</summary>
        public static readonly int Dom = 5;

        /// <summary>
        /// Maximum number of borrowed books per reader.
        /// </summary>
        public static readonly int C = 10;
    }
}
