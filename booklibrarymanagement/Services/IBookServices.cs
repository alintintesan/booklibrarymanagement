﻿// <copyright file="IBookServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>IBookServices interface..</summary>
    public interface IBookServices
    {
        /// <summary>Gets the list of books.</summary>
        /// <returns>The list of books.</returns>
        IList<Book> GetListOfBooks();

        /// <summary>Deletes the book.</summary>
        /// <param name="book">The book.</param>
        /// <returns>Operation successful or not.</returns>
        bool DeleteBook(Book book);

        /// <summary>Updates the book.</summary>
        /// <param name="book">The book.</param>
        /// <returns>Operation successful or not.</returns>
        bool UpdateBook(Book book);

        /// <summary>Gets the domain by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The book.</returns>
        Book GetBookById(int id);

        /// <summary>Adds the book.</summary>
        /// <param name="book">The book.</param>
        /// <returns>Operation successful or not.</returns>
        bool AddBook(Book book);
    }
}
