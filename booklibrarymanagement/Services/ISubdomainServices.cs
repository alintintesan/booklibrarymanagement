﻿// <copyright file="ISubdomainServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>ISubdomainServices interface.</summary>
    public interface ISubdomainServices
    {
        /// <summary>Gets the list of subdomains.</summary>
        /// <returns>The list of subdomains.</returns>
        IList<Subdomain> GetListOfSubdomains();

        /// <summary>Deletes the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        /// <returns>Operation successful or not.</returns>
        bool DeleteSubdomain(Subdomain subdomain);

        /// <summary>Updates the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        /// <returns>Operation successful or not.</returns>
        bool UpdateSubdomain(Subdomain subdomain);

        /// <summary>Gets the subdomain by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The subdomain.</returns>
        Subdomain GetSubdomainById(int id);

        /// <summary>Adds the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        /// <returns>Operation successful or not.</returns>
        bool AddSubdomain(Subdomain subdomain);
    }
}
