﻿// <copyright file="IReaderServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>IReaderServices interface.</summary>
    public interface IReaderServices
    {
        /// <summary>Gets the list of readers.</summary>
        /// <returns>The readers.</returns>
        IList<Reader> GetListOfReaders();

        /// <summary>Deletes the reader.</summary>
        /// <param name="reader">The reader.</param>
        /// <returns>Operation successful or not.</returns>
        bool DeleteReader(Reader reader);

        /// <summary>Updates the reader.</summary>
        /// <param name="reader">The reader.</param>
        /// <returns>Operation successful or not.</returns>
        bool UpdateReader(Reader reader);

        /// <summary>Gets the product by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The reader.</returns>
        Reader GetReaderById(int id);

        /// <summary>Adds the reader.</summary>
        /// <param name="reader">The reader.</param>
        /// <returns>Operation successful or not.</returns>
        bool AddReader(Reader reader);
    }
}
