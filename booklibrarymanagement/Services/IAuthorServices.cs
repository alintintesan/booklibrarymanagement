﻿// <copyright file="IAuthorServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>IAuthorServices interface.</summary>
    public interface IAuthorServices
    {
        /// <summary>Gets the list of authors.</summary>
        /// <returns>The list of authors.</returns>
        IList<Author> GetListOfAuthors();

        /// <summary>Deletes the author.</summary>
        /// <param name="author">The author.</param>
        /// <returns>Success or not.</returns>
        bool DeleteAuthor(Author author);

        /// <summary>Updates the author.</summary>
        /// <param name="author">The author.</param>
        /// <returns>Success or not.</returns>
        bool UpdateAuthor(Author author);

        /// <summary>Gets the author by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The author.</returns>
        Author GetAuthorById(int id);

        /// <summary>Adds the author.</summary>
        /// <param name="author">The author parameter.</param>
        /// <returns>The author.</returns>
        bool AddAuthor(Author author);
    }
}
