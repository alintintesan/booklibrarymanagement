﻿// <copyright file="IPublishingHouseServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>IPublishingHouseServices interface.</summary>
    public interface IPublishingHouseServices
    {
        /// <summary>Gets the list of houses.</summary>
        /// <returns>The list of houses.</returns>
        IList<PublishingHouse> GetListOfPublishingHouses();

        /// <summary>Deletes the house.</summary>
        /// <param name="house">The house.</param>
        /// <returns>Operation successful or not.</returns>
        bool DeletePublishingHouse(PublishingHouse house);

        /// <summary>Updates the house.</summary>
        /// <param name="house">The house.</param>
        /// <returns>Operation successful or not.</returns>
        bool UpdatePublishingHouse(PublishingHouse house);

        /// <summary>Gets the subdomain by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The publishing house.</returns>
        PublishingHouse GetPublishingHouseById(int id);

        /// <summary>Adds the house.</summary>
        /// <param name="house">The house.</param>
        /// <returns>Operation successful or not.</returns>
        bool AddPublishingHouse(PublishingHouse house);
    }
}
