﻿// <copyright file="IDomainServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>IDomainServices interface.</summary>
    public interface IDomainServices
    {
        /// <summary>Gets the list of domains.</summary>
        /// <returns>The list of domains.</returns>
        IList<Domain> GetListOfDomains();

        /// <summary>Deletes the domain.</summary>
        /// <param name="domain">The domain.</param>
        /// <returns>Operation successful or not.</returns>
        bool DeleteDomain(Domain domain);

        /// <summary>Updates the domain.</summary>
        /// <param name="domain">The domain.</param>
        /// <returns>Operation successful or not.</returns>
        bool UpdateDomain(Domain domain);

        /// <summary>Gets the domain by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The domain.</returns>
        Domain GetDomainById(int id);

        /// <summary>Adds the domain.</summary>
        /// <param name="domain">The domain.</param>
        /// <returns>Operation successful or not.</returns>
        bool AddDomain(Domain domain);
    }
}
