﻿// <copyright file="EditionServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services.ServicesImplementation
{
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using FluentValidation.Results;

    /// <summary>IEditionServices implementation.</summary>
    /// <seealso cref="BookLibraryManagement.Services.IEditionServices" />
    public class EditionServices : IEditionServices
    {
        /// <summary>The log.</summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(EditionServices));

        /// <summary>Gets or sets the data services.</summary>
        public static IEditionDataServices DataServices { get; set; } = DaoFactoryMethod.CurrentDAOFactory.EditionDataServices;

        /// <summary>Adds the edition.</summary>
        /// <param name="edition">The edition.</param>
        /// <returns>Operation successful or not.</returns>
        public bool AddEdition(Edition edition)
        {
            var validator = new EditionValidator();
            ValidationResult results = validator.Validate(edition);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The edition is valid!");
                DataServices.AddEdition(edition);
                Log.Info("The edition was added to the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The edition is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Deletes the edition.</summary>
        /// <param name="edition">The edition.</param>
        /// <returns>Operation successful or not.</returns>
        public bool DeleteEdition(Edition edition)
        {
            var validator = new EditionValidator();
            ValidationResult results = validator.Validate(edition);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The edition is valid!");
                DataServices.DeleteEdition(edition);
                Log.Info("The edition was deleted from the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The loan is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Gets the list of editions.</summary>
        /// <returns>List of loans.</returns>
        public IList<Edition> GetListOfEditions()
        {
            return DataServices.GetAllEditions();
        }

        /// <summary>Gets the edition by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The edition.</returns>
        public Edition GetEditionById(int id)
        {
            return DataServices.GetEditionById(id);
        }

        /// <summary>Updates the edition.</summary>
        /// <param name="edition">The edition.</param>
        /// <returns>Operation successful or not.</returns>
        public bool UpdateEdition(Edition edition)
        {
            var validator = new EditionValidator();
            ValidationResult results = validator.Validate(edition);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The edition is valid!");
                DataServices.UpdateEdition(edition);
                Log.Info("The edition was updated in the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The edition is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }
    }
}
