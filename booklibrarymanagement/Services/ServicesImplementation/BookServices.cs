﻿// <copyright file="BookServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services.ServicesImplementation
{
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using FluentValidation;
    using FluentValidation.Results;

    /// <summary>IBookServices implementation.</summary>
    /// <seealso cref="BookLibraryManagement.Services.IBookServices" />
    public class BookServices : IBookServices
    {
        /// <summary>The log.</summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(BookServices));

        /// <summary>Gets or sets the data services.</summary>
        public static IBookDataServices DataServices { get; set; } = DaoFactoryMethod.CurrentDAOFactory.BookDataServices;

        /// <summary>Gets or sets the validators.</summary>
        /// <value>The validators.</value>
        public static IList<AbstractValidator<Book>> Validators { get; set; } = new List<AbstractValidator<Book>>() { new BookValidator(), new BookComplexValidator() };

        /// <summary>Adds the book.</summary>
        /// <param name="book">The book parameter.</param>
        /// <returns>The book.</returns>
        public bool AddBook(Book book)
        {
            bool isValid = true;
            List<ValidationResult> validationResults = new List<ValidationResult>();

            foreach (var validator in Validators)
            {
                var result = validator.Validate(book);
                bool isValidated = result.IsValid;
                validationResults.Add(result);

                if (!isValidated)
                {
                    isValid = false;
                    break;
                }
            }

            if (isValid)
            {
                Log.Info("The book is valid!");
                DataServices.AddBook(book);
                Log.Info("The book was added to the database!");
            }
            else
            {
                Log.Error($"The book is not valid. The following errors occurred: {validationResults}");
            }

            return isValid;
        }

        /// <summary>Deletes the book.</summary>
        /// <param name="book">The book parameter.</param>
        /// <returns>The book.</returns>
        public bool DeleteBook(Book book)
        {
            bool isValid = true;
            List<ValidationResult> validationResults = new List<ValidationResult>();

            foreach (var validator in Validators)
            {
                var result = validator.Validate(book);
                bool isValidated = result.IsValid;
                validationResults.Add(result);

                if (!isValidated)
                {
                    isValid = false;
                    break;
                }
            }

            if (isValid)
            {
                Log.Info("The book is valid!");
                DataServices.DeleteBook(book);
                Log.Info("The book was deleted from the database!");
            }
            else
            {
                Log.Error($"The book is not valid. The following errors occurred: {validationResults}");
            }

            return isValid;
        }

        /// <summary>Gets the list of books.</summary>
        /// <returns>List of books.</returns>
        public IList<Book> GetListOfBooks()
        {
            return DataServices.GetAllBooks();
        }

        /// <summary>Gets the book by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The book.</returns>
        public Book GetBookById(int id)
        {
            return DataServices.GetBookById(id);
        }

        /// <summary>Updates the book.</summary>
        /// <param name="book">The book parameter.</param>
        /// <returns>The book.</returns>
        public bool UpdateBook(Book book)
        {
            bool isValid = true;
            List<ValidationResult> validationResults = new List<ValidationResult>();

            foreach (var validator in Validators)
            {
                var result = validator.Validate(book);
                bool isValidated = result.IsValid;
                validationResults.Add(result);

                if (!isValidated)
                {
                    isValid = false;
                    break;
                }
            }

            if (isValid)
            {
                Log.Info("The book is valid!");
                DataServices.UpdateBook(book);
                Log.Info("The book was updated in the database!");
            }
            else
            {
                Log.Error($"The book is not valid. The following errors occurred: {validationResults}");
            }

            return isValid;
        }
    }
}
