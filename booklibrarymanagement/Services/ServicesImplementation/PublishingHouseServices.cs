﻿// <copyright file="PublishingHouseServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services.ServicesImplementation
{
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using FluentValidation.Results;

    /// <summary>IPublishingHouseServices implementation.</summary>
    /// <seealso cref="BookLibraryManagement.Services.IPublishingHouseServices" />
    public class PublishingHouseServices : IPublishingHouseServices
    {
        /// <summary>The log.</summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(PublishingHouseServices));

        /// <summary>Gets or sets he data services.</summary>
        public static IPublishingHouseDataServices DataServices { get; set; } = DaoFactoryMethod.CurrentDAOFactory.PublishingHouseDataServices;

        /// <summary>Adds the house.</summary>
        /// <param name="house">The house.</param>
        /// <returns>Operation successful or not.</returns>
        public bool AddPublishingHouse(PublishingHouse house)
        {
            var validator = new PublishingHouseValidator();
            ValidationResult results = validator.Validate(house);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The house is valid!");
                DataServices.AddPublishingHouse(house);
                Log.Info("The house was added to the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The house is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Deletes the house.</summary>
        /// <param name="house">The house.</param>
        /// <returns>Operation successful or not.</returns>
        public bool DeletePublishingHouse(PublishingHouse house)
        {
            var validator = new PublishingHouseValidator();
            ValidationResult results = validator.Validate(house);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The house is valid!");
                DataServices.DeletePublishingHouse(house);
                Log.Info("The house was deleted from the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The house is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Gets the list of houses.</summary>
        /// <returns>List of houses.</returns>
        public IList<PublishingHouse> GetListOfPublishingHouses()
        {
            return DataServices.GetAllPublishingHouses();
        }

        /// <summary>Gets the house by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The house.</returns>
        public PublishingHouse GetPublishingHouseById(int id)
        {
            return DataServices.GetPublishingHouseById(id);
        }

        /// <summary>Updates the house.</summary>
        /// <param name="house">The house.</param>
        /// <returns>Operation successful or not.</returns>
        public bool UpdatePublishingHouse(PublishingHouse house)
        {
            var validator = new PublishingHouseValidator();
            ValidationResult results = validator.Validate(house);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The subdomain is valid!");
                DataServices.UpdatePublishingHouse(house);
                Log.Info("The subdomain was updated in the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The subdomain is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }
    }
}
