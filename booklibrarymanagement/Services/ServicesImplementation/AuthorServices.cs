﻿// <copyright file="AuthorServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services.ServicesImplementation
{
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using FluentValidation.Results;

    /// <summary>AuthorServices implementation.</summary>
    /// <seealso cref="BookLibraryManagement.Services.IAuthorServices" />
    public class AuthorServices : IAuthorServices
    {
        /// <summary>The log.</summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(AuthorServices));

        /// <summary>Gets or sets the data services.</summary>
        public static IAuthorDataServices DataServices { get; set; } = DaoFactoryMethod.CurrentDAOFactory.AuthorDataServices;

        /// <summary>Adds the author.</summary>
        /// <param name="author">The author parameter.</param>
        /// <returns>The author.</returns>
        public bool AddAuthor(Author author)
        {
            var validator = new AuthorValidator();
            ValidationResult results = validator.Validate(author);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The author is valid!");
                DataServices.AddAuthor(author);
                Log.Info("The author was added to the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The author is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Deletes the author.</summary>
        /// <param name="author">The author parameter.</param>
        /// <returns>The author.</returns>
        public bool DeleteAuthor(Author author)
        {
            var validator = new AuthorValidator();
            ValidationResult results = validator.Validate(author);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The author is valid!");
                DataServices.DeleteAuthor(author);
                Log.Info("The author was deleted from the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The author is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Gets the list of authors.</summary>
        /// <returns>List of authors.</returns>
        public IList<Author> GetListOfAuthors()
        {
            return DataServices.GettAllAuthors();
        }

        /// <summary>Gets the book by author.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The author.</returns>
        public Author GetAuthorById(int id)
        {
            return DataServices.GetAuthorById(id);
        }

        /// <summary>Updates the author.</summary>
        /// <param name="author">The author.</param>
        /// <returns>Successful operation or not.</returns>
        public bool UpdateAuthor(Author author)
        {
            var validator = new AuthorValidator();
            ValidationResult results = validator.Validate(author);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The author is valid!");
                DataServices.UpdateAuthor(author);
                Log.Info("The author was updated in the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The author is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }
    }
}
