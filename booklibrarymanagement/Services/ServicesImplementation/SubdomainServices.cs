﻿// <copyright file="SubdomainServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services.ServicesImplementation
{
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using FluentValidation.Results;

    /// <summary>ISubdomainServices implementation.</summary>
    /// <seealso cref="BookLibraryManagement.Services.ISubdomainServices" />
    public class SubdomainServices : ISubdomainServices
    {
        /// <summary>The log.</summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SubdomainServices));

        /// <summary>Gets or sets the data services.</summary>
        public static ISubdomainDataServices DataServices { get; set; } = DaoFactoryMethod.CurrentDAOFactory.SubdomainDataServices;

        /// <summary>Adds the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        /// <returns>Operation successful or not.</returns>
        public bool AddSubdomain(Subdomain subdomain)
        {
            var validator = new SubdomainValidator();
            ValidationResult results = validator.Validate(subdomain);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The subdomain is valid!");
                DataServices.AddSubdomain(subdomain);
                Log.Info("The subdomain was added to the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The subdomain is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Deletes the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        /// <returns>Operation successful or not.</returns>
        public bool DeleteSubdomain(Subdomain subdomain)
        {
            var validator = new SubdomainValidator();
            ValidationResult results = validator.Validate(subdomain);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The reader is valid!");
                DataServices.DeleteSubdomain(subdomain);
                Log.Info("The reader was deleted from the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The reader is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Gets the list of subdomains.</summary>
        /// <returns>List of subdomains.</returns>
        public IList<Subdomain> GetListOfSubdomains()
        {
            return DataServices.GetAllSubdomains();
        }

        /// <summary>Gets the subdomain by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The subdomain.</returns>
        public Subdomain GetSubdomainById(int id)
        {
            return DataServices.GetSubdomainById(id);
        }

        /// <summary>Updates the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        /// <returns>Operation successful or not.</returns>
        public bool UpdateSubdomain(Subdomain subdomain)
        {
            var validator = new SubdomainValidator();
            ValidationResult results = validator.Validate(subdomain);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The subdomain is valid!");
                DataServices.UpdateSubdomain(subdomain);
                Log.Info("The subdomain was updated in the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The subdomain is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }
    }
}
