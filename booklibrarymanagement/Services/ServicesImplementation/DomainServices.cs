﻿// <copyright file="DomainServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services.ServicesImplementation
{
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using FluentValidation.Results;

    /// <summary>IDomainServices implementation.</summary>
    /// <seealso cref="BookLibraryManagement.Services.IDomainServices" />
    public class DomainServices : IDomainServices
    {
        /// <summary>The log.</summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(DomainServices));

        /// <summary>Gets or sets the data services.</summary>
        public static IDomainDataServices DataServices { get; set; } = DaoFactoryMethod.CurrentDAOFactory.DomainDataServices;

        /// <summary>Adds the domain.</summary>
        /// <param name="domain">The domain.</param>
        /// <returns>Operation successful or not.</returns>
        public bool AddDomain(Domain domain)
        {
            var validator = new DomainValidator();
            ValidationResult results = validator.Validate(domain);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The domain is valid!");
                DataServices.AddDomain(domain);
                Log.Info("The domain was added to the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The domain is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Deletes the domain.</summary>
        /// <param name="domain">The domain.</param>
        /// <returns>Operation successful or not.</returns>
        public bool DeleteDomain(Domain domain)
        {
            var validator = new DomainValidator();
            ValidationResult results = validator.Validate(domain);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The domain is valid!");
                DataServices.DeleteDomain(domain);
                Log.Info("The domain was deleted from the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The domain is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Gets the list of domains.</summary>
        /// <returns>List of domains.</returns>
        public IList<Domain> GetListOfDomains()
        {
            return DataServices.GetAllDomains();
        }

        /// <summary>Gets the domain by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The domain.</returns>
        public Domain GetDomainById(int id)
        {
            return DataServices.GetDomainById(id);
        }

        /// <summary>Updates the domain.</summary>
        /// <param name="domain">The domain.</param>
        /// <returns>Operation successful or not.</returns>
        public bool UpdateDomain(Domain domain)
        {
            var validator = new DomainValidator();
            ValidationResult results = validator.Validate(domain);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The domain is valid!");
                DataServices.UpdateDomain(domain);
                Log.Info("The domain was updated in the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The domain is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }
    }
}
