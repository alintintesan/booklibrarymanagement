﻿// <copyright file="LoanServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services.ServicesImplementation
{
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using FluentValidation.Results;

    /// <summary>ILoanServices implementation.</summary>
    /// <seealso cref="BookLibraryManagement.Services.ILoanServices" />
    public class LoanServices : ILoanServices
    {
        /// <summary>The log.</summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(LoanServices));

        /// <summary>Gets or sets the data services.</summary>
        public static ILoanDataServices DataServices { get; set; } = DaoFactoryMethod.CurrentDAOFactory.LoanDataServices;

        /// <summary>Adds the loan.</summary>
        /// <param name="loan">The loan.</param>
        /// <returns>Operation successful or not.</returns>
        public bool AddLoan(Loan loan)
        {
            var validator = new LoanValidator();
            ValidationResult results = validator.Validate(loan);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The loan is valid!");
                DataServices.AddLoan(loan);
                Log.Info("The loan was added to the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The loan is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Deletes the loan.</summary>
        /// <param name="loan">The loan.</param>
        /// <returns>Operation successful or not.</returns>
        public bool DeleteLoan(Loan loan)
        {
            var validator = new LoanValidator();
            ValidationResult results = validator.Validate(loan);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The loan is valid!");
                DataServices.DeleteLoan(loan);
                Log.Info("The loan was deleted from the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The loan is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Gets the list of loans.</summary>
        /// <returns>List of loans.</returns>
        public IList<Loan> GetListOfLoans()
        {
            return DataServices.GetAllLoans();
        }

        /// <summary>Gets the loan by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The loan.</returns>
        public Loan GetLoanById(int id)
        {
            return DataServices.GetLoanById(id);
        }

        /// <summary>Updates the loan.</summary>
        /// <param name="loan">The loan.</param>
        /// <returns>Operation successful or not.</returns>
        public bool UpdateLoan(Loan loan)
        {
            var validator = new LoanValidator();
            ValidationResult results = validator.Validate(loan);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The loan is valid!");
                DataServices.UpdateLoan(loan);
                Log.Info("The loan was updated in the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The loan is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }
    }
}
