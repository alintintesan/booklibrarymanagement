﻿// <copyright file="ReaderServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services.ServicesImplementation
{
    using System.Collections.Generic;
    using BookLibraryManagement.DataMapper;
    using BookLibraryManagement.DomainModel;
    using FluentValidation.Results;

    /// <summary>IReaderServices implementation.</summary>
    /// <seealso cref="BookLibraryManagement.Services.IReaderServices" />
    public class ReaderServices : IReaderServices
    {
        /// <summary>The log.</summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ReaderServices));

        /// <summary>Gets or sets the data services.</summary>
        public static IReaderDataServices DataServices { get; set; } = DaoFactoryMethod.CurrentDAOFactory.ReaderDataServices;

        /// <summary>Adds the reader.</summary>
        /// <param name="reader">The reader.</param>
        /// <returns>Operation successful or not.</returns>
        public bool AddReader(Reader reader)
        {
            var validator = new ReaderValidator();
            ValidationResult results = validator.Validate(reader);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The reader is valid!");
                DataServices.AddReader(reader);
                Log.Info("The reader was added to the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The reader is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Deletes the reader.</summary>
        /// <param name="reader">The reader.</param>
        /// <returns>Operation successful or not.</returns>
        public bool DeleteReader(Reader reader)
        {
            var validator = new ReaderValidator();
            ValidationResult results = validator.Validate(reader);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The reader is valid!");
                DataServices.DeleteReader(reader);
                Log.Info("The reader was deleted from the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The reader is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }

        /// <summary>Gets the list of readers.</summary>
        /// <returns>List of readers.</returns>
        public IList<Reader> GetListOfReaders()
        {
            return DataServices.GetAllReaders();
        }

        /// <summary>Gets the product by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The reader.</returns>
        public Reader GetReaderById(int id)
        {
            return DataServices.GetReaderById(id);
        }

        /// <summary>Updates the reader.</summary>
        /// <param name="reader">The reader.</param>
        /// <returns>Operation successful or not.</returns>
        public bool UpdateReader(Reader reader)
        {
            var validator = new ReaderValidator();
            ValidationResult results = validator.Validate(reader);

            bool isValid = results.IsValid;

            if (isValid)
            {
                Log.Info("The reader is valid!");
                DataServices.UpdateReader(reader);
                Log.Info("The reader was updated in the database!");
            }
            else
            {
                IList<ValidationFailure> failures = results.Errors;
                Log.Error($"The reader is not valid. The following errors occurred: {failures}");
            }

            return isValid;
        }
    }
}
