﻿// <copyright file="IEditionServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>IEditionServices interface.</summary>
    public interface IEditionServices
    {
        /// <summary>Gets the list of editions.</summary>
        /// <returns>The list of editions.</returns>
        IList<Edition> GetListOfEditions();

        /// <summary>Deletes the edition.</summary>
        /// <param name="edition">The edition.</param>
        /// <returns>Operation successful or not.</returns>
        bool DeleteEdition(Edition edition);

        /// <summary>Updates the edition.</summary>
        /// <param name="edition">The edition.</param>
        /// <returns>Operation successful or not.</returns>
        bool UpdateEdition(Edition edition);

        /// <summary>Gets the edition by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The edition.</returns>
        Edition GetEditionById(int id);

        /// <summary>Adds the edition.</summary>
        /// <param name="edition">The edition.</param>
        /// <returns>Operation successful or not.</returns>
        bool AddEdition(Edition edition);
    }
}
