﻿// <copyright file="ILoanServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.Services
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>ILoanServices interface.</summary>
    public interface ILoanServices
    {
        /// <summary>Gets the list of loans.</summary>
        /// <returns>The list of loans.</returns>
        IList<Loan> GetListOfLoans();

        /// <summary>Deletes the loan.</summary>
        /// <param name="loan">The loan.</param>
        /// <returns>Operation successful or not.</returns>
        bool DeleteLoan(Loan loan);

        /// <summary>Updates the loan.</summary>
        /// <param name="loan">The loan.</param>
        /// <returns>Operation successful or not.</returns>
        bool UpdateLoan(Loan loan);

        /// <summary>Gets the loan by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The loan.</returns>
        Loan GetLoanById(int id);

        /// <summary>Adds the loan.</summary>
        /// <param name="loan">The loan.</param>
        /// <returns>Operation successful or not.</returns>
        bool AddLoan(Loan loan);
    }
}
