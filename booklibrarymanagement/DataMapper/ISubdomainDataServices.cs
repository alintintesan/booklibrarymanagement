﻿// <copyright file="ISubdomainDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>Subdomain data services interface.</summary>
    public interface ISubdomainDataServices
    {
        /// <summary>Gets all subdomains.</summary>
        /// <returns>All subdomains.</returns>
        IList<Subdomain> GetAllSubdomains();

        /// <summary>Adds the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        void AddSubdomain(Subdomain subdomain);

        /// <summary>Deletes the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        void DeleteSubdomain(Subdomain subdomain);

        /// <summary>Updates the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        void UpdateSubdomain(Subdomain subdomain);

        /// <summary>Gets the subdomain by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The subdomain.</returns>
        Subdomain GetSubdomainById(int id);
    }
}
