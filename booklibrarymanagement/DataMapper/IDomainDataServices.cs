﻿// <copyright file="IDomainDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>Domain data services interface.</summary>
    public interface IDomainDataServices
    {
        /// <summary>Gets all domains.</summary>
        /// <returns>All domains.</returns>
        IList<Domain> GetAllDomains();

        /// <summary>Adds the domain.</summary>
        /// <param name="domain">The domain.</param>
        void AddDomain(Domain domain);

        /// <summary>Deletes the domain.</summary>
        /// <param name="domain">The domain.</param>
        void DeleteDomain(Domain domain);

        /// <summary>Updates the domain.</summary>
        /// <param name="domain">The domain.</param>
        void UpdateDomain(Domain domain);

        /// <summary>Gets the domain by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The domain.</returns>
        Domain GetDomainById(int id);
    }
}
