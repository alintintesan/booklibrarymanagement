﻿// <copyright file="IReaderDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>Reader data services interface.</summary>
    public interface IReaderDataServices
    {
        /// <summary>Gets all readers.</summary>
        /// <returns>All readers.</returns>
        IList<Reader> GetAllReaders();

        /// <summary>Adds the reader.</summary>
        /// <param name="reader">The reader.</param>
        void AddReader(Reader reader);

        /// <summary>Deletes the reader.</summary>
        /// <param name="reader">The reader.</param>
        void DeleteReader(Reader reader);

        /// <summary>Updates the reader.</summary>
        /// <param name="reader">The reader.</param>
        void UpdateReader(Reader reader);

        /// <summary>Gets the reader by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The reader.</returns>
        Reader GetReaderById(int id);
    }
}
