﻿// <copyright file="IAuthorDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    using BookLibraryManagement.DomainModel;
    using System.Collections.Generic;

    /// <summary>Author data services interface.</summary>
    public interface IAuthorDataServices
    {
        /// <summary>Getts all authors.</summary>
        /// <returns>All authors.</returns>
        IList<Author> GettAllAuthors();

        /// <summary>Adds the author.</summary>
        /// <param name="author">The author.</param>
        void AddAuthor(Author author);

        /// <summary>Deletes the author.</summary>
        /// <param name="author">The author.</param>
        void DeleteAuthor(Author author);

        /// <summary>Updates the author.</summary>
        /// <param name="author">The author.</param>
        void UpdateAuthor(Author author);

        /// <summary>Gets the author by identifier.</summary>
        /// <param name="id">The identifier.</param>
        Author GetAuthorById(int id);
    }
}
