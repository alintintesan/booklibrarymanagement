﻿// <copyright file="IDaoFactory.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    /// <summary>Dao Factory interface.</summary>
    public interface IDaoFactory
    {
        /// <summary>Gets the author data services.</summary>
        /// <value>The author data services.</value>
        IAuthorDataServices AuthorDataServices { get; }

        /// <summary>Gets the book data services.</summary>
        /// <value>The book data services.</value>
        IBookDataServices BookDataServices { get; }

        /// <summary>Gets the domain data services.</summary>
        /// <value>The domain data services.</value>
        IDomainDataServices DomainDataServices { get; }

        /// <summary>Gets the edition data services.</summary>
        /// <value>The edition data services.</value>
        IEditionDataServices EditionDataServices { get; }

        /// <summary>Gets the loan data services.</summary>
        /// <value>The loan data services.</value>
        ILoanDataServices LoanDataServices { get; }

        /// <summary>Gets the publishing house data services.</summary>
        /// <value>The publishing house data services.</value>
        IPublishingHouseDataServices PublishingHouseDataServices { get; }

        /// <summary>Gets the reader data services.</summary>
        /// <value>The reader data services.</value>
        IReaderDataServices ReaderDataServices { get; }

        /// <summary>Gets the subdomain data services.</summary>
        /// <value>The subdomain data services.</value>
        ISubdomainDataServices SubdomainDataServices { get; }
    }
}
