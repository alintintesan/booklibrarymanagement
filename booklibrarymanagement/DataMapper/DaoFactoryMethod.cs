﻿// <copyright file="DaoFactoryMethod.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    using BookLibraryManagement.Configuration;
    using BookLibraryManagement.DataMapper.SqlServerDAO;

    /// <summary>
    /// DaoFactoryMethod implementation.
    /// </summary>
    public class DaoFactoryMethod
    {
        /// <summary>Gets the current DAO factory.</summary>
        /// <value>The current DAO factory.</value>
        public static IDaoFactory CurrentDAOFactory
        {
            get
            {
                string currentDataProvider = Config.DataProvider;

                if (string.IsNullOrWhiteSpace(currentDataProvider))
                {
                    return null;
                }
                else
                {
                    switch (currentDataProvider.ToLower().Trim())
                    {
                        case "sqlserver":
                            return new SqlServerDaoFactory();

                        default:
                            return new SqlServerDaoFactory();
                    }
                }
            }
        }
    }
}
