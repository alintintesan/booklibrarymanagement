﻿// <copyright file="IEditionDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>Edition data services interface.</summary>
    public interface IEditionDataServices
    {
        /// <summary>Gets all editions.</summary>
        /// <returns>All editions.</returns>
        IList<Edition> GetAllEditions();

        /// <summary>Adds the edition.</summary>
        /// <param name="edition">The edition.</param>
        void AddEdition(Edition edition);

        /// <summary>Deletes the edition.</summary>
        /// <param name="edition">The edition.</param>
        void DeleteEdition(Edition edition);

        /// <summary>Updates the edition.</summary>
        /// <param name="edition">The edition.</param>
        void UpdateEdition(Edition edition);

        /// <summary>Gets the edition by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The edition.</returns>
        Edition GetEditionById(int id);
    }
}
