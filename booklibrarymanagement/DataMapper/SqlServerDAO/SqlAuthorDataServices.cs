﻿// <copyright file="SqlAuthorDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using BookLibraryManagement.DomainModel;
    
    /// <summary>IAuthorDataServices implementation.</summary>
    /// <seealso cref="BookLibraryManagement.DataMapper.IAuthorDataServices" />
    public class SqlAuthorDataServices : IAuthorDataServices
    {
        /// <summary>Adds the author.</summary>
        /// <param name="author">The author.</param>
        public void AddAuthor(Author author)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Authors.Add(author);
                context.SaveChanges();
            }
        }

        /// <summary>Deletes the author.</summary>
        /// <param name="author">The author.</param>
        public void DeleteAuthor(Author author)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Author toBeDeleted = new Author { Id = author.Id };
                context.Authors.Attach(toBeDeleted);
                context.Authors.Remove(toBeDeleted);
                context.SaveChanges();
            }
        }

        /// <summary>Gets all authors.</summary>
        /// <returns>All authors.</returns>
        public IList<Author> GettAllAuthors()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                return context.Authors.Select(author => author).ToList();
            }
        }

        /// <summary>Updates the author.</summary>
        /// <param name="author">The author.</param>
        public void UpdateAuthor(Author author)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Author toBeUpdated = context.Authors.Find(author.Id);

                if (toBeUpdated == null)
                {
                    return;
                }
                context.Entry(toBeUpdated).CurrentValues.SetValues(author);
                context.SaveChanges();
            }
        }

        /// <summary>Gets the author by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The author.</returns>
        public Author GetAuthorById(int id)
        {
            using (var context = new ApplicationContext())
            {
                return context.Authors.Where(author => author.Id == id).SingleOrDefault();
            }
        }
    }
}
