﻿// <copyright file="SqlDomainDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using BookLibraryManagement.DomainModel;
    
    /// <summary>IDomainDataServices interface implementation.</summary>
    /// <seealso cref="BookLibraryManagement.DataMapper.IDomainDataServices" />
    public class SqlDomainDataServices : IDomainDataServices
    {
        /// <summary>Adds the domain.</summary>
        /// <param name="domain">The domain.</param>
        public void AddDomain(Domain domain)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Domains.Add(domain);
                context.SaveChanges();
            }
        }

        /// <summary>Deletes the domain.</summary>
        /// <param name="domain">The domain.</param>
        public void DeleteDomain(Domain domain)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Domain toBeDeleted = new Domain { Id = domain.Id };
                context.Domains.Attach(toBeDeleted);
                context.Domains.Remove(toBeDeleted);
                context.SaveChanges();
            }
        }

        /// <summary>Gets all domains.</summary>
        /// <returns>All domains.</returns>
        public IList<Domain> GetAllDomains()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                return context.Domains.Select(domain => domain).ToList();
            }
        }

        /// <summary>Updates the domain.</summary>
        /// <param name="domain">The domain.</param>
        public void UpdateDomain(Domain domain)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Domain toBeUpdated = context.Domains.Find(domain.Id);

                if (toBeUpdated == null)
                {
                    return;
                }

                context.Entry(toBeUpdated).CurrentValues.SetValues(domain);
                context.SaveChanges();
            }
        }

        /// <summary>Gets the domain by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The domain.</returns>
        public Domain GetDomainById(int id)
        {
            using (var context = new ApplicationContext())
            {
                return context.Domains.Where(domain => domain.Id == id).SingleOrDefault();
            }
        }
    }
}
