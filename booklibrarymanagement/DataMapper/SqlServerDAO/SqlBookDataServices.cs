﻿// <copyright file="SqlBookDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using BookLibraryManagement.DomainModel;
    
    /// <summary>IBookDataServices interface implementation.</summary>
    /// <seealso cref="BookLibraryManagement.DataMapper.IBookDataServices" />
    public class SqlBookDataServices : IBookDataServices
    {
        /// <summary>Adds the book.</summary>
        /// <param name="book">The author.</param>
        public void AddBook(Book book)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Books.Add(book);
                context.SaveChanges();
            }
        }

        /// <summary>Deletes the book.</summary>
        /// <param name="book">The book.</param>
        public void DeleteBook(Book book)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Book toBeDeleted = new Book { Id = book.Id };
                context.Books.Attach(toBeDeleted);
                context.Books.Remove(toBeDeleted);
                context.SaveChanges();
            }
        }

        /// <summary>Gets all books.</summary>
        /// <returns>All books.</returns>
        public IList<Book> GetAllBooks()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                return context.Books.Select(book => book).ToList();
            }
        }

        /// <summary>Updates the book.</summary>
        /// <param name="book">The book.</param>
        public void UpdateBook(Book book)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Book toBeUpdated = context.Books.Find(book.Id);

                if (toBeUpdated == null)
                {
                    return;
                }

                context.Entry(toBeUpdated).CurrentValues.SetValues(book);
                context.SaveChanges();
            }
        }

        /// <summary>Gets the book by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The book.</returns>
        public Book GetBookById(int id)
        {
            using (var context = new ApplicationContext())
            {
                return context.Books.Where(book => book.Id == id).SingleOrDefault();
            }
        }
    }
}
