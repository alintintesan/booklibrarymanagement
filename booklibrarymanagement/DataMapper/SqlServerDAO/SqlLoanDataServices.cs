﻿// <copyright file="SqlLoanDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using BookLibraryManagement.DomainModel;

    /// <summary>ILoanDataServices interface implementation.</summary>
    /// <seealso cref="BookLibraryManagement.DataMapper.ILoanDataServices" />
    public class SqlLoanDataServices : ILoanDataServices
    {
        /// <summary>Adds the loan.</summary>
        /// <param name="loan">The loan.</param>
        public void AddLoan(Loan loan)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Loans.Add(loan);
                context.SaveChanges();
            }
        }

        /// <summary>Deletes the loan.</summary>
        /// <param name="loan">The loan.</param>
        public void DeleteLoan(Loan loan)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Loan toBeDeleted = new Loan { Id = loan.Id };
                context.Loans.Attach(toBeDeleted);
                context.Loans.Remove(toBeDeleted);
                context.SaveChanges();
            }
        }

        /// <summary>Gets all loans.</summary>
        /// <returns>All loans.</returns>
        public IList<Loan> GetAllLoans()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                return context.Loans.Select(loan => loan).ToList();
            }
        }

        /// <summary>Updates the loan.</summary>
        /// <param name="loan">The loan.</param>
        public void UpdateLoan(Loan loan)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Loan toBeUpdated = context.Loans.Find(loan.Id);

                if (toBeUpdated == null)
                {
                    return;
                }

                context.Entry(toBeUpdated).CurrentValues.SetValues(loan);
                context.SaveChanges();
            }
        }

        /// <summary>Gets the loan by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The loan.</returns>
        public Loan GetLoanById(int id)
        {
            using (var context = new ApplicationContext())
            {
                return context.Loans.Where(loan => loan.Id == id).SingleOrDefault();
            }
        }
    }
}
