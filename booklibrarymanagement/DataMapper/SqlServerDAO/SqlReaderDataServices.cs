﻿// <copyright file="SqlReaderDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using BookLibraryManagement.DomainModel;
    
    /// <summary>IReaderDataServices interface implementation.</summary>
    /// <seealso cref="BookLibraryManagement.DataMapper.IReaderDataServices" />
    public class SqlReaderDataServices : IReaderDataServices
    {
        /// <summary>Adds the reader.</summary>
        /// <param name="reader">The reader.</param>
        public void AddReader(Reader reader)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Readers.Add(reader);
                context.SaveChanges();
            }
        }

        /// <summary>Deletes the reader.</summary>
        /// <param name="reader">The reader.</param>
        public void DeleteReader(Reader reader)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Reader toBeDeleted = new Reader { Id = reader.Id };
                context.Readers.Attach(toBeDeleted);
                context.Readers.Remove(toBeDeleted);
                context.SaveChanges();
            }
        }

        /// <summary>Gets all readers.</summary>
        /// <returns>All readers.</returns>
        public IList<Reader> GetAllReaders()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                return context.Readers.Select(reader => reader).ToList();
            }
        }

        /// <summary>Updates the reader.</summary>
        /// <param name="reader">The reader.</param>
        public void UpdateReader(Reader reader)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Reader toBeUpdated = context.Readers.Find(reader.Id);

                if (toBeUpdated == null)
                {
                    return;
                }

                context.Entry(toBeUpdated).CurrentValues.SetValues(reader);
                context.SaveChanges();
            }
        }

        /// <summary>Gets the loan by reader.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The reader.</returns>
        public Reader GetReaderById(int id)
        {
            using (var context = new ApplicationContext())
            {
                return context.Readers.Where(reader => reader.Id == id).SingleOrDefault();
            }
        }
    }
}
