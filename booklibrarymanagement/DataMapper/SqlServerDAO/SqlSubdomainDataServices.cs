﻿// <copyright file="SqlSubdomainDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using BookLibraryManagement.DomainModel;

    /// <summary>ISubdomainDataServices interface implementation.</summary>
    /// <seealso cref="BookLibraryManagement.DataMapper.ISubdomainDataServices" />
    public class SqlSubdomainDataServices : ISubdomainDataServices
    {
        /// <summary>Adds the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        public void AddSubdomain(Subdomain subdomain)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Subdomains.Add(subdomain);
                context.SaveChanges();
            }
        }

        /// <summary>Deletes the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        public void DeleteSubdomain(Subdomain subdomain)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Subdomain toBeDeleted = new Subdomain { Id = subdomain.Id };
                context.Subdomains.Attach(toBeDeleted);
                context.Subdomains.Remove(toBeDeleted);
                context.SaveChanges();
            }
        }

        /// <summary>Gets all subdomains.</summary>
        /// <returns>All subdomains.</returns>
        public IList<Subdomain> GetAllSubdomains()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                return context.Subdomains.Select(subdomain => subdomain).ToList();
            }
        }

        /// <summary>Updates the subdomain.</summary>
        /// <param name="subdomain">The subdomain.</param>
        public void UpdateSubdomain(Subdomain subdomain)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Subdomain toBeUpdated = context.Subdomains.Find(subdomain.Id);

                if (toBeUpdated == null)
                {
                    return;
                }

                context.Entry(toBeUpdated).CurrentValues.SetValues(subdomain);
                context.SaveChanges();
            }
        }

        /// <summary>Gets the subdomain by id.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The subdomain.</returns>
        public Subdomain GetSubdomainById(int id)
        {
            using (var context = new ApplicationContext())
            {
                return context.Subdomains.Where(subdomain => subdomain.Id == id).SingleOrDefault();
            }
        }
    }
}
