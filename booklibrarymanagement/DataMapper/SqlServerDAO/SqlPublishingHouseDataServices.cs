﻿// <copyright file="SqlPublishingHouseDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using BookLibraryManagement.DomainModel;

    /// <summary>IPublishingHouseDataServices interface implementation.</summary>
    /// <seealso cref="BookLibraryManagement.DataMapper.IPublishingHouseDataServices" />
    public class SqlPublishingHouseDataServices : IPublishingHouseDataServices
    {
        /// <summary>Adds the house.</summary>
        /// <param name="house">The house.</param>
        public void AddPublishingHouse(PublishingHouse house)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                context.PublishingHouses.Add(house);
                context.SaveChanges();
            }
        }

        /// <summary>Deletes the house.</summary>
        /// <param name="house">The house.</param>
        public void DeletePublishingHouse(PublishingHouse house)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                PublishingHouse toBeDeleted = new PublishingHouse { Id = house.Id };
                context.PublishingHouses.Attach(toBeDeleted);
                context.PublishingHouses.Remove(toBeDeleted);
                context.SaveChanges();
            }
        }

        /// <summary>Gets all houses.</summary>
        /// <returns>All houses.</returns>
        public IList<PublishingHouse> GetAllPublishingHouses()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                return context.PublishingHouses.Select(house => house).ToList();
            }
        }

        /// <summary>Updates the house.</summary>
        /// <param name="house">The house.</param>
        public void UpdatePublishingHouse(PublishingHouse house)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                PublishingHouse toBeUpdated = context.PublishingHouses.Find(house.Id);

                if (toBeUpdated == null)
                {
                    return;
                }

                context.Entry(toBeUpdated).CurrentValues.SetValues(house);
                context.SaveChanges();
            }
        }

        /// <summary>Gets the subdomain by id.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The publishing house.</returns>
        public PublishingHouse GetPublishingHouseById(int id)
        {
            using (var context = new ApplicationContext())
            {
                return context.PublishingHouses.Where(house => house.Id == id).SingleOrDefault();
            }
        }
    }
}
