﻿// <copyright file="SqlEditionDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using BookLibraryManagement.DomainModel;
    
    /// <summary>IEditionDataServices interface implementation.</summary>
    /// <seealso cref="BookLibraryManagement.DataMapper.IEditionDataServices" />
    public class SqlEditionDataServices : IEditionDataServices
    {
        /// <summary>Adds the edition.</summary>
        /// <param name="edition">The edition.</param>
        public void AddEdition(Edition edition)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                context.Editions.Add(edition);
                context.SaveChanges();
            }
        }

        /// <summary>Deletes the edition.</summary>
        /// <param name="edition">The edition.</param>
        public void DeleteEdition(Edition edition)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Edition toBeDeleted = new Edition { Id = edition.Id };
                context.Editions.Attach(toBeDeleted);
                context.Editions.Remove(toBeDeleted);
                context.SaveChanges();
            }
        }

        /// <summary>Gets all editions.</summary>
        /// <returns>All editions.</returns>
        public IList<Edition> GetAllEditions()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                return context.Editions.Select(edition => edition).ToList();
            }
        }

        /// <summary>Updates the edition.</summary>
        /// <param name="edition">The edition.</param>
        public void UpdateEdition(Edition edition)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                Edition toBeUpdated = context.Editions.Find(edition.Id);

                if (toBeUpdated == null)
                {
                    return;
                }

                context.Entry(toBeUpdated).CurrentValues.SetValues(edition);
                context.SaveChanges();
            }
        }

        /// <summary>Gets the edition by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The edition.</returns>
        public Edition GetEditionById(int id)
        {
            using (var context = new ApplicationContext())
            {
                return context.Editions.Where(edition => edition.Id == id).SingleOrDefault();
            }
        }
    }
}
