﻿// <copyright file="ApplicationContext.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    using System.Data.Entity;
    using BookLibraryManagement.DomainModel;

    /// <summary>ApplicationContext class.</summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    public class ApplicationContext : DbContext
    {
        /// <summary>The log.</summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ApplicationContext));

        /// <summary>Initializes a new instance of the <see cref="ApplicationContext"/> class.</summary>
        public ApplicationContext() : base("connString")
        {
            Log.Info("ApplicationContext instance created!");
        }

        /// <summary>Gets or sets the authors.</summary>
        /// <value>The authors.</value>
        public DbSet<Author> Authors { get; set; }

        /// <summary>Gets or sets the books.</summary>
        /// <value>The books.</value>
        public DbSet<Book> Books { get; set; }

        /// <summary>Gets or sets the domains.</summary>
        /// <value>The domains.</value>
        public DbSet<Domain> Domains { get; set; }

        /// <summary>Gets or sets the subdomains.</summary>
        /// <value>The subdomains.</value>
        public DbSet<Subdomain> Subdomains { get; set; }

        /// <summary>Gets or sets the editions.</summary>
        /// <value>The editions.</value>
        public DbSet<Edition> Editions { get; set; }

        /// <summary>Gets or sets the loans.</summary>
        /// <value>The loans.</value>
        public DbSet<Loan> Loans { get; set; }

        /// <summary>Gets or sets the publishing houses.</summary>
        /// <value>The publishing houses.</value>
        public DbSet<PublishingHouse> PublishingHouses { get; set; }

        /// <summary>Gets or sets the readers.</summary>
        /// <value>The readers.</value>
        public DbSet<Reader> Readers { get; set; }
    }
}
