﻿// <copyright file="SqlServerDaoFactory.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper.SqlServerDAO
{
    /// <summary>IDaoFactory implementation.</summary>
    /// <seealso cref="BookLibraryManagement.DataMapper.IDaoFactory" />
    public class SqlServerDaoFactory : IDaoFactory
    {
        /// <summary>Gets the author data services.</summary>
        /// <value>The author data services.</value>
        public IAuthorDataServices AuthorDataServices { get => new SqlAuthorDataServices(); }

        /// <summary>Gets the book data services.</summary>
        /// <value>The book data services.</value>
        public IBookDataServices BookDataServices { get => new SqlBookDataServices(); }

        /// <summary>Gets the domain data services.</summary>
        /// <value>The domain data services.</value>
        public IDomainDataServices DomainDataServices { get => new SqlDomainDataServices(); }

        /// <summary>Gets the edition data services.</summary>
        /// <value>The edition data services.</value>
        public IEditionDataServices EditionDataServices { get => new SqlEditionDataServices(); }

        /// <summary>Gets the loan data services.</summary>
        /// <value>The loan data services.</value>
        public ILoanDataServices LoanDataServices { get => new SqlLoanDataServices(); }

        /// <summary>Gets the publishing house data services.</summary>
        /// <value>The publishing house data services.</value>
        public IPublishingHouseDataServices PublishingHouseDataServices { get => new SqlPublishingHouseDataServices(); }

        /// <summary>Gets the reader data services.</summary>
        /// <value>The reader data services.</value>
        public IReaderDataServices ReaderDataServices { get => new SqlReaderDataServices(); }

        /// <summary>Gets the subdomain data services.</summary>
        /// <value>The subdomain data services.</value>
        public ISubdomainDataServices SubdomainDataServices { get => new SqlSubdomainDataServices(); }
    }
}
