﻿// <copyright file="IBookDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>Book data services interface.</summary>
    public interface IBookDataServices
    {
        /// <summary>Gets all books.</summary>
        /// <returns>All books.</returns>
        IList<Book> GetAllBooks();

        /// <summary>Adds the book.</summary>
        /// <param name="book">The book.</param>
        void AddBook(Book book);

        /// <summary>Deletes the book.</summary>
        /// <param name="book">The book.</param>
        void DeleteBook(Book book);

        /// <summary>Updates the book.</summary>
        /// <param name="book">The book.</param>
        void UpdateBook(Book book);

        /// <summary>Gets the book by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The book.</returns>
        Book GetBookById(int id);
    }
}
