﻿// <copyright file="ILoanDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>Loan data services interface.</summary>
    public interface ILoanDataServices
    {
        /// <summary>Gets all loans.</summary>
        /// <returns>All loans.</returns>
        IList<Loan> GetAllLoans();

        /// <summary>Adds the loan.</summary>
        /// <param name="loan">The loan.</param>
        void AddLoan(Loan loan);

        /// <summary>Deletes the loan.</summary>
        /// <param name="loan">The loan.</param>
        void DeleteLoan(Loan loan);

        /// <summary>Updates the loan.</summary>
        /// <param name="loan">The loan.</param>
        void UpdateLoan(Loan loan);

        /// <summary>Gets the loan by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The loan.</returns>
        Loan GetLoanById(int id);
    }
}
