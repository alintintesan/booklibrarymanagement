﻿// <copyright file="IPublishingHouseDataServices.cs" company="Transilvania University of Brasov">
// Alin Tintesan
// </copyright>

namespace BookLibraryManagement.DataMapper
{
    using System.Collections.Generic;
    using BookLibraryManagement.DomainModel;

    /// <summary>Publishing house data services interface.</summary>
    public interface IPublishingHouseDataServices
    {
        /// <summary>Gets all publishing houses.</summary>
        /// <returns>All publishing houses.</returns>
        IList<PublishingHouse> GetAllPublishingHouses();

        /// <summary>Adds the publishing house.</summary>
        /// <param name="house">The publishing house.</param>
        void AddPublishingHouse(PublishingHouse house);

        /// <summary>Deletes the publishing house.</summary>
        /// <param name="house">The publishing house.</param>
        void DeletePublishingHouse(PublishingHouse house);

        /// <summary>Updates the publishing house.</summary>
        /// <param name="house">The publishing house.</param>
        void UpdatePublishingHouse(PublishingHouse house);

        /// <summary>Gets the publishing house by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The house.</returns>
        PublishingHouse GetPublishingHouseById(int id);
    }
}
